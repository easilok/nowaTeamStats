<?php

	//include('./assets/misc/misc.inc');
	
	if (isset($_POST['statsPlayer'])) {		
			
		$statsPlayer = strip_tags(trim($_POST['statsPlayer']));
		$statsGoals = strip_tags(trim($_POST['statsGoals']));
		$statsAssists = strip_tags(trim($_POST['statsAssists']));
		$statsOwnGoals = strip_tags(trim($_POST['statsOwnGoals']));
		$statsObs = strip_tags(trim($_POST['statsObs']));
		$statsPayed = 0;
		if (isset($_POST['statsPayed'])){
			if ($_POST['statsPayed'] == "True"){
				$statsPayed = 1;
			}
		}
		$statsVictory = 0; 
		if (isset($_POST['statsVictory'])){
			if ($_POST['statsVictory'] == "True"){
				$statsVictory = 1;
			}
		}		
		
		$statsColor = strip_tags(trim($_POST['statsColor']));
		/*
		$connection = new PDO("mysql:host=$host;dbname=$database;charset=utf8",$user,$password);
		$connection->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES 'UTF8'");
		*/
		include_once("functions.php");
		$connection = getDatabaseConnection();
		$query = $connection->query("Select COUNT(STA_Player) from tblStats where STA_Player = '$statsPlayer' and STA_GameDate = '$statsDate'");
		
		
		if (! $query) {
			echo "<br><p class=\"redInformation\"> Erro de Execução:</p><br>\n";
			$errorMessage = $connection->errorInfo();
			writeErrorLog($errorMessage);
		}
		else {
			$count = $query->fetchColumn();
					
			if ($count > 0 ) {
				echo "<br><p class=\"redInformation\"> Estatística para o Mágico Já está registada. Escolher outro Mágico.</p><br>";
			}
			
			else {

				$query = $connection->prepare("INSERT INTO tblStats (STA_Player,STA_GameDate,STA_Goals,STA_Assists,STA_OwnGoals,STA_Obs, STA_Payed,STA_Color, STA_Victory) VALUES (:statsPlayer,:statsDate,:statsGoals,:statsAssists,:statsOwnGoals,:statsObs,:statsPayed,:statsColor,:statsVictory)");
				$numRows = $query->execute(array(
					"statsPlayer" => $statsPlayer,
					"statsDate" => $statsDate,
					"statsGoals" => $statsGoals,
					"statsAssists" => $statsAssists,
					"statsOwnGoals" => $statsOwnGoals,
					"statsObs" =>  utf8_decode($statsObs),
					"statsPayed" => $statsPayed,
					"statsColor" => utf8_decode($statsColor),
					"statsVictory" => $statsVictory
				));
				
				if (!$numRows) {
					echo "<br><p class=\"redInformation\"> Erro ao Inserir Estatística. </p><br>";
					$errorMessage = $query->errorInfo();
					writeErrorLog($errorMessage);	
				}
				else {

					echo "<br><p class=\"redInformation\"> Inserida Estatística. </p><br>";
					
					$statsPlayer = null;
					$statsGoals = 0;
					$statsAssists = 0;
					$statsOwnGoals = 0;
					$statsObs = "";
					$statsPayed = True;
					$statsVictory = False;
					$statsColor = "Preto";

					$username = @$_SESSION['user'];
					writeDataBaseLog($username, 'Inserir', 'Inserida Estatística '.$statsPlayer.", ".$statsDate, 1);
				}
			}
		}
	}
		
?>