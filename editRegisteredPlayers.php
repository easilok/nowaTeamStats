<?php
	session_start();
	if ((@$_SESSION['auth'] != "yes") or (@$_SESSION['group'] < 4)){
		header("Location: index.php");
		exit();
	}		
?>
<!DOCTYPE HTML>
<!--
	Helios by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>NowaTeam: Editar Manager</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<link rel="icon" href="./images/Nowabrand.png">
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body class="no-sidebar">
		<div id="page-wrapper">

			<!-- Header -->
				<div id="header">

					<!-- Inner -->
						<div class="inner">
							<header>
								<h1><a href="index.php" id="logo">NowaTeam</a></h1>
							</header>
						</div>

					<!-- Nav -->
					<?php include 'navigationMenu.php'; ?>

				</div>

			<!-- Main -->
				<div class="wrapper style1">

					<div class="container">
					<?php  include('./assets/includes/users/viewEditUser1.html'); ?>
						<article id="main" class="special">
							<header>
								<h2><a href="#">Editar o Mágico <?php echo $_POST['playerName']."<br>"; ?></a></h2>
							</header>
							<p>
								<?php
									include('functions.php');
									
									if (isset($_POST['btnAction'])){
										if (($_POST['btnAction'] == "Guardar")  or  ($_POST['btnAction'] == "photo"))
										{
											$editPlayerId = $_POST['playerId'];
											$editPlayerName = $_POST['playerName'];
											$editPlayerEmail = $_POST['playerEmail'];
											$editPlayerPhone = $_POST['playerPhone'];
											$editPlayerQuote = $_POST['playerQuote'];
											$editPlayerObs = $_POST['playerObs'];
											$editAction = "photo";
											$editType = True;
											$editUsername = "";
										
											if (strlen($editPlayerName) > 1) {
												$tempPlayerName = $editPlayerName;
											}
											else {
												$tempPlayerName = "na";
											}										
											$editPlayerPhoto =  loadUserPhoto($editType, $_POST['playerPhoto'], $editPlayerId, $tempPlayerName);//$_POST['playerPhoto'];
											
											$result = array(
												"PLY_Name" => utf8_decode($editPlayerName),
												"PLY_Email" => $editPlayerEmail,
												"PLY_Phone" => $editPlayerPhone,
												"PLY_Quote" => utf8_decode($editPlayerQuote),
												"PLY_Obs" => utf8_decode($editPlayerObs),
												"PLY_Id" => $editPlayerId,
												"PLY_Photo" => $editPlayerPhoto
											);
											if ($_POST['btnAction'] == "Guardar") {
												saveMagicDetails($result);
											}
										}
									}
									else{
										$row = getMagicInfo($_POST['playerId']);
										$editPlayerName = utf8_encode($row['PLY_Name']);
										$editPlayerEmail = $row['PLY_Email'];
										$editPlayerPhone = $row['PLY_Phone'];
										$editPlayerQuote = utf8_encode($row['PLY_Quote']);
										$editPlayerId = $row['PLY_Id'];
										$editPlayerObs = utf8_encode($row['PLY_Obs']);
										$editPlayerPhoto = $row['PLY_Photo'];
										$editAction = "photo";
										$editType = True;
										$editUsername = "";
										
										$result = array(
											"PLY_Name" => utf8_decode($editPlayerName),
											"PLY_Email" => $editPlayerEmail,
											"PLY_Phone" => $editPlayerPhone,
											"PLY_Quote" => utf8_decode($editPlayerQuote),
											"PLY_Obs" => utf8_decode($editPlayerObs),
											"PLY_Id" => $editPlayerId,
											"PLY_Photo" => $editPlayerPhoto
										);
										
									}
								?>
								<form action="editRegisteredPlayers.php" method="POST">
									<fieldset>
									
										<?php listUserDetails(true, $result , true, false); ?>
										
									</fieldset>
									<br>
								</form>
							</p>							
						</article>
						<?php
							include('./assets/includes/users/viewEditUser2.html');
							$formPost = "editRegisteredPlayers.php";
							include('fillPlayerImage.php');
							include('./assets/includes/users/viewEditUser3.html');
						?>						
					</div>

				</div>

			<!-- Footer -->
			<?php include 'footerInclude.php'; ?>
		
		</div>

		<?php include_once('./assets/includes/utils/incScripts.php'); ?>

	</body>
</html>