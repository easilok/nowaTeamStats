<?php
		//include('./assets/misc/misc.inc');
				
		if (isset($_POST['userPassword'])){
			
			$username = @$_SESSION['user'];//$_POST['userUsername'];
			if (isset($userUsername)) {$username = $userUsername;}
			
			if ($username == "") {header("Location: viewUser.php");}
			
			$userPassword = $_POST['userPassword'];
			$userPasswordRepeat = $_POST['userPasswordRepeat'];
			
			if (strlen($userPassword) < 6) {
				echo "<br><p class=\"redInformation\"> A palavra passe deve conter mais de 6 caracteres</p><br>";	
			}
			else {
				if ($userPassword == $userPasswordRepeat){
					/*
					$connection = new PDO("mysql:host=$host;dbname=$database;charset=utf8",$user,$password);
					$connection->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES 'UTF8'");
					*/
					include_once("functions.php");
					$connection = getDatabaseConnection();				
					$query = $connection->query("Select COUNT(USE_id) from tblUser where USE_USERNAME = '$username'");
				
					if (! $query) {
						echo "<br><p class=\"redInformation\"> Erro de Execução:</p><br>\n";
						$errorMessage = $connection->errorInfo();
						writeErrorLog($errorMessage);	
					}
					else {
						$count = $query->fetchColumn();
						
						/* Utilizador existente*/
						if ($count <= 0 ) {
							echo "<br><p class=\"redInformation\"> Utilizador não existe. </p><br>";
						}
						else {
							$query = $connection->prepare("update tblUser SET USE_Pass = :userPassword, USE_DateModify = :userDateModify WHERE USE_USERNAME = :username");
						
							$numRows = $query->execute(array(
								"username" => utf8_decode($username),
								"userPassword" => hash('sha512',$userPassword),
								"userDateModify" => date("Y-m-d"),
							));	
							
							if (!$numRows) {
								echo "<br><p class=\"redInformation\"> Erro ao Atualizar palavra passe de Utilizador: $username </p><br>";
								$errorMessage = $query->errorInfo();
								writeErrorLog($errorMessage);	
							}
							else {

								echo "<br><p class=\"redInformation\"> Palavra passe de Utilizador: $username alterada com sucesso.</p><br>";
								
								writeDataBaseLog($username, 'Alterar', 'Alterada Palavra-Passe'.$username, 1);								
							}
						}
					}
				}
				else {
					echo "<br><p class=\"redInformation\"> Palavras Passe não coincidem.</p><br>";
				}
			}
		}
?>