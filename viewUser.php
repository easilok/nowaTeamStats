<?php
	session_start();
	if (@$_SESSION['auth'] != "yes"){
		header("Location: login.php");
		exit();
	}		
	$sessionUser = @$_SESSION['user'];
?>
<!DOCTYPE HTML>
<!--
	Helios by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>NowaTeam <?php echo $sessionUser; ?></title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<link rel="stylesheet" href="assets/css/styles.css" />
		<link rel="icon" href="./images/Nowabrand.png">
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body class="no-sidebar">
		<div id="page-wrapper">

			<!-- Header -->
				<div id="header">

					<!-- Inner -->
						<div class="inner">
							<header>
								<h1><a href="index.php" id="logo">NowaTeam</a></h1>
							</header>
						</div>

					<!-- Nav -->
					<?php include 'navigationMenu.php'; ?>

				</div>

			<!-- Main -->
			<?php include('listUser.php'); ?>
			
				<div class="wrapper style1">
					<div class="container">
					<?php if ($hasPlayer) { include('./assets/includes/users/viewEditUser1.html');}
						//<div class="row 200%">
						//<!-- Lado esquerdo-->
							//<div class="8u 12u(mobile)" id="content">
							?>
								<article id="main" class="special">
									<header>
										<h2><a href="#">Detalhes do Manager <?php echo $sessionUser; ?></a></h2>
									</header>
									<p>
										<form action="editUser.php" method="POST">
											<fieldset>
											
											<?php /*include('listUser.php');*/ ?>
											<?php
												include_once('functions.php');
												listUserDetails(False, $row, $hasPlayer, True);
											?>
												
											</fieldset>
											<br>
										</form>
									</p>							
								</article>
								<?php 
									if ($hasPlayer) { 
										include('./assets/includes/users/viewEditUser2.html');
										$playerPhoto = $row['PLY_Photo'];
										echo "<IMG SRC=\"$playerPhoto\" ALT=\"Foto do Mágico\" WIDTH=100% >";	
										include('./assets/includes/users/viewEditUser3.html');
									}
						?>
					</div>

				</div>

			<!-- Footer -->
			<?php include 'footerInclude.php'; ?>
		
		</div>

		<?php include_once('./assets/includes/utils/incScripts.php'); ?>

	</body>
</html>