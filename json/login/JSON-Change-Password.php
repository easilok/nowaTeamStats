<?php

	//include_once($_SERVER['DOCUMENT_ROOT'].'/functions.php');
	$BASE_DIR = '../../';
	include_once($BASE_DIR.'/functions.php');
	
	$jsonArray = array(
		'Changed' => False, 
		'PasswordSize' => False,
		'userNotFound' => False,
		'diffPassword' => False,
		'Error' => False,
		'noData' => False,
		);

	if (isset($_POST['userPassword']) and isset($_POST['userUsername'])){
		
		$username = $_POST['userUsername'];		
		$userPassword = $_POST['userPassword'];
		$userPasswordRepeat = $_POST['userPasswordRepeat'];
		
		if (strlen($userPassword) < 6) {
			$jsonArray['PasswordSize'] = true;	
		}
		else {
			if ($userPassword == $userPasswordRepeat){

				$connection = getDatabaseConnection();				
				$query = $connection->query("Select COUNT(USE_id) from tblUser where USE_USERNAME = '$username'");
			
				if (!$query) {
					$errorMessage = $connection->errorInfo();
					writeErrorLog($errorMessage);	
					$jsonArray['Error'] = true;
				}
				else {
					$count = $query->fetchColumn();
					
					/* Utilizador existente*/
					if ($count <= 0 ) {
						$jsonArray['userNotFound'] = true;
					}
					else {
						$query = $connection->prepare("update tblUser SET USE_Pass = :userPassword, USE_DateModify = :userDateModify, USE_PassExpired = 0 WHERE USE_USERNAME = :username");
					
						$numRows = $query->execute(array(
							"username" => utf8_decode($username),
							"userPassword" => hash('sha512',$userPassword),
							"userDateModify" => date("Y-m-d"),
						));	
						
						if (!$numRows) {
							$errorMessage = $query->errorInfo();
							writeErrorLog($errorMessage);	
							$jsonArray['Error'] = true;
						}
						else {
							$jsonArray['Changed'] = true;
							writeDataBaseLog($username, 'Alterar', 'Alterada Palavra-Passe de '.$username, 1);
						}
					}
				}
			}
			else {
				$jsonArray['diffPassword'] = true;
			}
		}
	}

	echo json_encode($jsonArray);
	$query = null;
	$connection = null;
?>