<?php

	//include_once($_SERVER['DOCUMENT_ROOT'].'/functions.php');
	$BASE_DIR = '../../';
	include_once($BASE_DIR.'/functions.php');
	
	$jsonArray = array(
		'sent' => False, 
		'invalidEmail' => False,
		'userNotFound' => False,
		'Error' => False,
		'noData' => False,
		);

	//No incoming Data
	if (isset($_POST['userUsername']) and isset($_POST['recoverEmail'])){
		$username = $_POST['userUsername'];
		$email = $_POST['recoverEmail'];

		
		if ($username == "") { //Invalid User
			$jsonArray['Error'] = True;	
			$jsonArray['noData'] = True;
		} elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) { //Invalid Email
			$jsonArray['invalidEmail'] = True;
		} else { //Check if user Exists
			$connection = getDatabaseConnection();				
			$query = $connection->query("Select COUNT(USE_id) from tblUser where USE_USERNAME = '$username'");
		
			if (!$query) {
				$errorMessage = $connection->errorInfo();
				writeErrorLog($errorMessage);	
				$jsonArray['Error'] = true;
			}
			else {
				$count = $query->fetchColumn();
				
				if ($count <= 0 ) { //User not found
					$jsonArray['userNotFound'] = true;
				} else {

					//Generate Random Password
				    
				    //These 2 variables get pass as argument in a function
				    $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
				    $length = 8;

				    $userPassword = '';
				    $max = mb_strlen($keyspace, '8bit') - 1;
				    for ($i = 0; $i < $length; ++$i) {
				        //$userPassword .= $keyspace[random_int(0, $max)];
				        $userPassword .= $keyspace[rand(0, $max)];
				    }

				    
					$query = $connection->prepare("update tblUser SET USE_Pass = :userPassword, USE_DateModify = :userDateModify, USE_PassExpired = 1 WHERE USE_USERNAME = :username");
				
					$numRows = $query->execute(array(
						"username" => utf8_decode($username),
						"userPassword" => hash('sha512',$userPassword),
						"userDateModify" => date("Y-m-d"),
					));	
					

					$numRows = 5; //RETIRAR
					if (!$numRows) {
						$errorMessage = $query->errorInfo();
						writeErrorLog($errorMessage);	
						$jsonArray['Error'] = true;
					}
					else {
						//Password Changed
						//Send Email

						$to = $email;

						// subject
						$subject = 'NowateamStatistics Email Service';

						// message
						$message = '<html><body>';
						$message .= '<h1>NowateamStatistics recuperação de Palavra-Passe!!</h1><br><br>';
						$message .= 'A Sua nova Palavra-Passe é: <strong>'.$userPassword.'</strong>.<br><br>';
						$message .= 'Efetue login com a nova Palavra-Passe e será solicitada alteração de Palavra-Passe.<br><br>';
						//$message .= '<a href="http://nowateamstats.net16.net/login.php">NowateamStatistics</a>';
						$message .= '</body></html>';	

						// headers
						//$headers .= $email;
						$headers = "From: NowateamStatistics <no-reply@nowateamstats.net16.net>\r\n";
						$headers .= "MIME-Version: 1.0\r\n";
						$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

						// Mail it
						mail($to, $subject, $message, $headers);

						$jsonArray['sent'] = true;
						writeDataBaseLog($username, 'Alterar', 'Recuperação Palavra-Passe de '.$username, 1);
					}
				}
			}			
		}
	}
	else {
		$jsonArray['Error'] = True;		
		$jsonArray['noData'] = True;		
	}

	echo json_encode($jsonArray);
	$query = null;
	$connection = null;
?>