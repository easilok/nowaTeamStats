<?php
	session_start();
	
	//include_once($_SERVER['DOCUMENT_ROOT'].'/functions.php');
	$BASE_DIR = '../../';
	include_once($BASE_DIR.'/functions.php');
	
	$jsonArray = array(
		'Login' => False, 
		'PasswordExpired' => False,
		'LoginNotFound' => False,
		'Error' => False,
		'noData' => False,
		);

	if (isset($_POST['username'])){
		
		$username = $_POST['username'];
		$userPassword = $_POST['userPassword'];
		
		$connection = getDatabaseConnection();
		
		$userPasswordEncoded = hash('sha512',$userPassword);
		
		$query = $connection->prepare("Select COUNT(USE_id) from tblUser where USE_USERNAME = :username and USE_Pass = :userPassword and USE_Active = 1");
	
		$query->execute(array(
			'username' => $username, 
			'userPassword' => $userPasswordEncoded,
			));

		if (!$query) {
			$errorMessage = $connection->errorInfo();
			writeErrorLog($errorMessage, "...");								
			$jsonArray['Error'] = True;
		}
		else {
			$count = $query->fetchColumn();
					
			if ($count > 0 ) {			

				$query = $connection->prepare("Select USE_PassExpired from tblUser where USE_USERNAME = :username");
			
				$query->execute(array(
					'username' => $username, 
					));

				if ($query->fetchColumn() == 0) {
					$_SESSION['auth'] = 'yes';
					$_SESSION['user'] = $username;
					$_SESSION['group'] = getUserGroup($username) ;
					$jsonArray['Login'] = True;
					writeDataBaseLog($username, 'Acesso', 'Login de '.$username, 1);
				}
				else {
					$jsonArray['PasswordExpired'] = True;
				}
			}
			else {
				$jsonArray['LoginNotFound'] = True;
			}
		}
	}
	else {
		$jsonArray['Error'] = True;		
		$jsonArray['noData'] = True;		
	}

	echo json_encode($jsonArray);
	$query = null;
	$connection = null;
?>