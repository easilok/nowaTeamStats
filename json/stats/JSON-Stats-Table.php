<?php

	//include_once($_SERVER['DOCUMENT_ROOT']."/functions.php");
	$BASE_DIR = '../../';
	include_once($BASE_DIR.'/functions.php');
	
	$type = 0;
	$strWhere = "ROUND(SUM(STA_Goals)/COUNT(STA_GameDate),2) DESC";

	$fieldType = $_POST['fieldType'];
	$fieldOrder = $_POST['fieldOrder'];
	$fieldSequence = $_POST['fieldSequence'];

	$strWhere = getStatsOrder($fieldOrder, $fieldType, $fieldSequence);

	$connection = getDatabaseConnection();

	$stringQuery =  "SELECT @curRow := @curRow + 1 AS 'Posicao' ,  t1.PLY_Name as 'Magico', ";
	if ($type == 0) {
		$stringQuery = $stringQuery."Coalesce(oldQuery.oldPosition,1000) as 'Posicao_Ant', ";
		
	}
	$stringQuery = $stringQuery."Batalhas, Golos, Golos_Rat, Assistencias, Assistencias_Rat , Vitorias, Vitorias_Rat";
	if ($type == 0) {
		$stringQuery = $stringQuery.", Traicoes, Traicoes_Rat, Assiduidade ";
	}
	$stringQuery = $stringQuery."FROM
						( SELECT
						PLY_NAME , COUNT(STA_GameDate) as 'Batalhas',
						SUM(STA_Goals) as 'Golos', ROUND(SUM(STA_Goals)/COUNT(STA_GameDate),2) as 'Golos_Rat', 
						SUM(STA_Assists) as 'Assistencias', ROUND(SUM(STA_Assists)/COUNT(STA_GameDate),2) as 'Assistencias_Rat',
						SUM(STA_Victory = 1) as 'Vitorias', ROUND(SUM(STA_Victory = 1)/COUNT(STA_GameDate),2) as 'Vitorias_Rat',
						SUM(STA_OwnGoals) as 'Traicoes', ROUND(SUM(STA_OwnGoals)/COUNT(STA_GameDate),2) as 'Traicoes_Rat',
						CONCAT(ROUND(100 * (COUNT(STA_GameDate)/TotalJogos.Jogos),0),'%') as 'Assiduidade', 
						ROUND(100 * (COUNT(STA_GameDate)/TotalJogos.Jogos),0) as tempAss
						FROM tblStats
						inner Join tblPlayer on PLY_ID = STA_Player
						inner Join (SELECT COUNT(DISTINCT STA_GameDate) as 'Jogos' FROM tblStats) as TotalJogos
						GROUP BY PLY_NAME
						HAVING tempAss >= 25
						ORDER BY $strWhere, PLY_NAME ASC
						) t1";
	if ($type == 0) {
		$stringQuery = $stringQuery." LEFT OUTER JOIN (
						SELECT @oldRow := @oldRow + 1 AS oldPosition, playerName FROM
						( SELECT
						PLY_NAME as playerName,
						ROUND(100 * (COUNT(STA_GameDate)/TotalJogos.Jogos),0) as tempAss
						FROM tblStats
						inner Join tblPlayer on PLY_ID = STA_Player
						inner Join (SELECT COUNT(DISTINCT STA_GameDate) as 'Jogos' FROM tblStats) as TotalJogos
						WHERE STA_GameDate < (SELECT MAX(STA_GameDate) FROM tblStats)
						GROUP BY PLY_NAME
						HAVING tempAss >= 25
						ORDER BY $strWhere, PLY_NAME ASC
						) t3, (SELECT @oldRow:=0) t4
						) oldQuery
						on playerName = PLY_Name";
	}
	$stringQuery = $stringQuery.", (SELECT @curRow:=0) t2";

	//echo $stringQuery;

	$query = $connection->query($stringQuery);
	
	if (! $query) {
		$errorMessage = $connection->errorInfo();
		writeErrorLog($errorMessage);	
		$jsonArray = array(
		'Error' => True, 
		);
	}
	else {
		
		$column_count = $query->columnCount();
		$result = $query->fetchALL();
		$rowCounter = 0;

		foreach ($result as $row) {
			for ($counter = 0; $counter < $column_count; $counter++) {
				$meta = $query->getColumnMeta($counter);
				$jsonArray[$rowCounter][$meta['name']] = utf8_encode($row[$counter]);
			}
			$rowCounter++;
		}
		echo json_encode($jsonArray);
	}
	
	$query = null;
	$connection = null;
?>