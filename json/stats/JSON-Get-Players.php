<?php

	$BASE_DIR = '../../';
	include_once($BASE_DIR.'/functions.php');

	$statsDate = @$_POST['statsDate'];

	$queryWhere = '';

	if ($statsDate <> '') {
		$queryWhere = "WHERE PLY_id NOT IN (SELECT STA_Player FROM tblStats WHERE STA_GameDate = '$statsDate')";
	}

	$connection = getDatabaseConnection();

	// TODO: Ordenar Por Assiduidade
	$query = $connection->query("Select PLY_ID, PLY_Name from tblPlayer $queryWhere ORDER BY PLY_Name");
	
	if (!$query) {
		$errorMessage = $connection->errorInfo();
		writeErrorLog($errorMessage);		
		$jsonArray = array(
		'Error' => True, 
		);				
	}
	else {
		$result = $query->fetchALL();
		
		$rowCounter = 0;
		foreach ($result as $row)
		{
			$playerID =$row['PLY_ID'];
			$playerName = utf8_encode($row['PLY_Name']);		
			
			$jsonArray[$rowCounter]['PLY_ID'] = $playerID;
			$jsonArray[$rowCounter]['PLY_Name'] = $playerName;

			$rowCounter++;
		}

		if ($rowCounter == 0) {
			$jsonArray = array();
		}
		echo json_encode($jsonArray);
	}

	$query = null;
	$connection = null;

?>