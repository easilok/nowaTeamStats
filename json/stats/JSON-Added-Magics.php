<?php

	$BASE_DIR = '../../';
	include_once($BASE_DIR.'/functions.php');

	$statsDate = @$_POST['statsDate'];

	if ($statsDate <> "") {

		$connection = getDatabaseConnection();
		$query = $connection->query("SELECT PLY_Name FROM tblPlayer INNER JOIN tblStats ON STA_Player = PLY_ID WHERE STA_GameDate = '$statsDate' ORDER BY PLY_Name");

		if (! $query) {
			$errorMessage = $connection->errorInfo();
			writeErrorLog($errorMessage);	
			$jsonArray = array(
			'Error' => True, 
			);					
		}
		else {
			$result = $query->fetchALL();
			
			$rowCounter = 0;
			foreach ($result as $row)
			{
				$playerName = utf8_encode($row['PLY_Name']);			

				$jsonArray[$rowCounter]['PLY_Name'] = $playerName;

				$rowCounter++;				
			}
			if ($rowCounter == 0) {
				$jsonArray = array();
			}			
			echo json_encode($jsonArray);
		}
		
		$query = null;
		$connection = null;

	}
?>