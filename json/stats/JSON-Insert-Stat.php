<?php

	//include_once($_SERVER['DOCUMENT_ROOT'].'/functions.php');
	$BASE_DIR = '../../';
	include_once($BASE_DIR.'/functions.php');
	
	$jsonArray = array(
		'Added' => False, 
		'AlreadyExists' => False,
		'Error' => False,
		'noData' => False,
		);

	if (isset($_POST['statsPlayer'])) {		
		
		$statsDate = strip_tags(trim($_POST['statsDate']));	
		$statsPlayer = strip_tags(trim($_POST['statsPlayer']));
		$statsGoals = strip_tags(trim($_POST['statsGoals']));
		$statsAssists = strip_tags(trim($_POST['statsAssists']));
		$statsOwnGoals = strip_tags(trim($_POST['statsOwnGoals']));
		$statsObs = strip_tags(trim($_POST['statsObs']));
		$statsPayed = $_POST['statsPayed'];
		$statsVictory = $_POST['statsVictory']; 
		$statsColor = strip_tags(trim($_POST['statsColor']));

		if (($statsPlayer <> "") and ($statsDate <> "")) {

			$connection = getDatabaseConnection();
			$query = $connection->query("Select COUNT(STA_Player) from tblStats where STA_Player = '$statsPlayer' and STA_GameDate = '$statsDate'");
			
			
			if (! $query) {
				$jsonArray['Error'] = true;
				$errorMessage = $connection->errorInfo();
				writeErrorLog($errorMessage);
			}
			else {
				$count = $query->fetchColumn();
						
				if ($count > 0 ) {
					$jsonArray['AlreadyExists'] = true;
					$jsonArray['Error'] = true;
				}
				
				else {

					$query = $connection->prepare("INSERT INTO tblStats (STA_Player,STA_GameDate,STA_Goals,STA_Assists,STA_OwnGoals,STA_Obs, STA_Payed,STA_Color, STA_Victory) VALUES (:statsPlayer,:statsDate,:statsGoals,:statsAssists,:statsOwnGoals,:statsObs,:statsPayed,:statsColor,:statsVictory)");
					$numRows = $query->execute(array(
						"statsPlayer" => $statsPlayer,
						"statsDate" => $statsDate,
						"statsGoals" => $statsGoals,
						"statsAssists" => $statsAssists,
						"statsOwnGoals" => $statsOwnGoals,
						"statsObs" =>  utf8_decode($statsObs),
						"statsPayed" => $statsPayed,
						"statsColor" => utf8_decode($statsColor),
						"statsVictory" => $statsVictory
					));
					
					if (!$numRows) {
						$errorMessage = $query->errorInfo();
						writeErrorLog($errorMessage);	
						$jsonArray['Error'] = true;
					}
					else {

						$jsonArray['Added'] = true;
						
						$statsPlayer = null;
						$statsGoals = 0;
						$statsAssists = 0;
						$statsOwnGoals = 0;
						$statsObs = "";
						$statsPayed = True;
						$statsVictory = False;
						$statsColor = "Preto";


						//writeDataBaseLog($username, 'Inserir', 'Inserida Estatística '.$statsPlayer.", ".$statsDate, 1);
					}
				}
			}
		}
		else {
			$jsonArray['noData'] = true;
			$jsonArray['Error'] = true;
		}
	} 

	echo json_encode($jsonArray);
	$query = null;
	$connection = null;
?>