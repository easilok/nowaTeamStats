<?php

	$BASE_DIR = '../../';
	include_once($BASE_DIR.'/functions.php');

	$statsDate = @$_POST['statsDate'];
	$statsColor = @$_POST['statsColor'];

	$queryWhere = '';
	$orderQuery = ' ORDER BY STA_Goals ASC';

	if ($statsDate <> '') {
		$queryWhere = "WHERE STA_GameDate = '$statsDate'";
	} else {
		$queryWhere = "WHERE STA_GameDate = (Select Max(STA_GameDate) From tblStats)";
	}

	if (($statsColor == 'Preto') OR ($statsColor == 'Branco')) {
		$queryWhere .= "AND STA_Color = '$statsColor'";
		if ($statsColor == 'Branco') {
			$orderQuery = ' ORDER BY STA_Goals DESC';
		}
	} else {
		$queryWhere .= "AND STA_Color = 'Preto'";
	}

	$connection = getDatabaseConnection();

	// TODO: Ordenar Por Assiduidade
	$query = $connection->query(
		"select STA_Player, STA_GameDate, PLY_Name, STA_Goals,
		STA_Assists, STA_OwnGoals from tblStats
		inner join tblPlayer on STA_Player = PLY_Id ".$queryWhere.$orderQuery);
	
	if (!$query) {
		$errorMessage = $connection->errorInfo();
		writeErrorLog($errorMessage);		
		$jsonArray = array(
		'Error' => True, 
		);				
	}
	else {
		$result = $query->fetchALL();
		
		$rowCounter = 0;
		foreach ($result as $row)
		{
			$statsPlayer =$row['STA_Player'];
			$statsDate = $row['STA_GameDate'];
			$playerName = utf8_encode($row['PLY_Name']);
			$statsGoals = $row['STA_Goals'];
			$statsAssists = $row['STA_Assists'];
			$statsOwn = $row['STA_OwnGoals'];
			
			$jsonArray[$rowCounter]['STA_Player'] = $statsPlayer;
			$jsonArray[$rowCounter]['STA_GameDate'] = $statsDate;
			$jsonArray[$rowCounter]['PLY_Name'] = $playerName;
			$jsonArray[$rowCounter]['STA_Goals'] = $statsGoals;
			$jsonArray[$rowCounter]['STA_Assists'] = $statsAssists;
			$jsonArray[$rowCounter]['STA_OwnGoals'] = $statsOwn;
			
			$rowCounter++;
		}

		if ($rowCounter == 0) {
			$jsonArray = array();
		}
		echo json_encode($jsonArray);
	}

	$query = null;
	$connection = null;

?>