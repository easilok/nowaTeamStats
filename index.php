<?php
	session_start();
?>

<!DOCTYPE HTML>	
<!--
	Helios by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>NowaTeam Statistics</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<link rel="stylesheet" href="assets/css/styles.css" />
		<link rel="icon" href="./images/Nowabrand.png">
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body class="homepage">
		<div id="page-wrapper">

			<!-- Header -->
				<div id="header" class="frontPage">

					<!-- Inner -->
						<div class="inner">
							<header>
								<h1><a href="index.php" id="logo">NowaTeam</a></h1>
								<hr />
								<p>Equipa de nobres e esplendorosos jogadores, 
								<br>que magnificamente demonstram a sua magia sábado após sábado, 
								<br>no mítico campo de batalha da Madalena</p>
								<!--<p>Estatísticas, Mágicos, Jornadas</p>-->
							</header>
							<footer>
								<!--<a href="#banner" class="button circled scrolly">Batalha</a>-->
								<a href="#main" class="button circled scrolly">Estatisticas</a>
								<!--<a href="#features" class="button circled scrolly">Mágicos</a>-->
							</footer>
						</div>

					<!-- Nav -->
					<?php include 'navigationMenu.php'; ?>

				</div>

			<!-- Banner -->
				<section id="banner">
					<header>
						<h2>Campo de Batalha</h2>
						<p> A Próxima batalha está marcada para dia ...
						<br> As equipas são as seguintes:
						</p>
						<hr class="inbanner"/>
							<table class="noborder" >
								<tr>
									<td>
										<strong>Pretos:</strong>		
									</td>
									<td>
									<strong>Brancos:</strong>
									</td> 
								</tr>
							</table>
						<hr class="inbanner"/>
					</header>
				</section>

			<!-- Main -->
				<div class="wrapper style2">

					<article id="main" class="container special">
						<!--<a href="#" class="image featured"><img src="images/pic06.jpg" alt="" /></a>-->
						<header>
							<h2><a href="#">Estatísticas dos Mágicos em Batalha</a></h2>
						</header>
							<div class="ovScroll">
								<?php 
									include_once('functions.php');
									getStatsTable(1," ROUND(SUM(STA_Goals)/COUNT(STA_GameDate),2) DESC");
								?>
							</div>
						<footer>
							<a href="statistics.php" class="button">Ver em detalhe</a>
						</footer>						
					</article>

				</div>

			<!-- Features -->
				<div class="wrapper style1">

					<section id="features" class="container special">
						<header>
							<h2>Os Mágicos dos Relvados</h2>
							<p>E as suas emblemáticas palavras de magia.</p>
						</header>
						<?php include('listPlayersPhotos.php'); ?>
					</section>

				</div>

			<!-- Footer -->
			<?php include 'footerInclude.php'; ?>
			
		</div>

		<?php include_once('./assets/includes/utils/incScripts.php'); ?>
			
	</body>
</html>