<?php
	session_start();
	if (@$_SESSION['auth'] != "yes"){
		header("Location: login.php");
		exit();
	}		
	$sessionUser = @$_SESSION['user'];
?>
<!DOCTYPE HTML>
<!--
	Helios by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>NowaTeam <?php echo $sessionUser; ?></title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<link rel="icon" href="./images/Nowabrand.png">
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body class="no-sidebar">
		<div id="page-wrapper">

			<!-- Header -->
				<div id="header">

					<!-- Inner -->
						<div class="inner">
							<header>
								<h1><a href="index.php" id="logo">NowaTeam</a></h1>
							</header>
						</div>

					<!-- Nav -->
					<?php include 'navigationMenu.php'; ?>

				</div>

			<?php 
				//include_once("functions.php");
				//Para que a nova imagem seja usada em ambos os lados
							
				//if ($_POST['hasPlayer']) {
				//	$_POST['playerPhoto'] = loadUserPhoto();
				//}
				
				include('performEditUser.php');				
			?>				
				
			<!-- Main -->
				<div class="wrapper style1">
					<div class="container">
						<?php if ($editType) { include('./assets/includes/users/viewEditUser1.html');} ?>
								<article id="main" class="special">
									<header>
										<h2><a href="#">Detalhes do Manager <?php echo $sessionUser; ?></a></h2>
									</header>
									<p>
										<form action="editUser.php" method="POST">
											<fieldset>
											<?php 
												listUserDetails(true, $result , $editType, true);
											?>
											</fieldset>
											<br>
										</form>
									</p>							
								</article>
								<?php 
									if ($editType) { 
										include('./assets/includes/users/viewEditUser2.html');		
										$formPost = "editUser.php";
										include('fillPlayerImage.php');
										include('./assets/includes/users/viewEditUser3.html');
									}
								?>																		
					</div>
				</div>

			<!-- Footer -->
			<?php include 'footerInclude.php'; ?>
		
		</div>

		<?php include_once('./assets/includes/utils/incScripts.php'); ?>

	</body>
</html>