<?php
	session_start();
?>
<!DOCTYPE HTML>
<!--
	Helios by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>NowaTeam Estatísticas Detalhadas</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<link rel="stylesheet" href="assets/css/styles.css" />
		<link rel="stylesheet" href="assets/css/jquery.tzCheckbox.css" />
		<link rel="icon" href="./images/Nowabrand.png">
		<link rel="stylesheet" href="assets/css/font-awesome.min.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body class="no-sidebar">
		<div id="page-wrapper">

			<!-- Header -->
				<div id="header">

					<!-- Inner -->
						<div class="inner">
							<header>
								<h1><a href="index.php" id="logo">NowaTeam</a></h1>
							</header>
						</div>

					<!-- Nav -->
					<?php include 'navigationMenu.php'; ?>

				</div>

			<!-- Main -->
				<div class="wrapper style1">

					<div class="container">
						<article id="main" class="special">
						<header>
							<h2 id="statsTitle">Estatísticas  Gerais dos Mágicos</h2>
						</header>
						<br>
						<label for="ch_effects" class="tzLabel">Ordenar Tabela por:</label><input type="checkbox" id="ch_effects" name="ch_effects" data-on="Rácio" data-off="Absoluto" checked />
						<input type="hidden" id="order-type" value = "racio" />
						<br>
							<div class="ovScroll">
								<table id="full-table-stats" class="default"></table>
							</div>
						</article>
						<article id="main-off-stats" class="special"></article>
					</div>

				</div>

			<!-- Footer -->
			<?php include 'footerInclude.php'; ?>
		
		</div>

		<?php include_once('./assets/includes/utils/incScripts.php'); ?>
	    <script src="./assets/js/ajax-utils.js"></script>
	    <script src="./assets/js/function-utils.js"></script>
		<script type='text/javascript' src='./assets/js/jquery.tzCheckbox.js'></script>
	    <script src="./assets/js/script-statistics.js"></script>	

	</body>
</html>