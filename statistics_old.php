<?php
	session_start();
?>
<!DOCTYPE HTML>
<!--
	Helios by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>NowaTeam Estatísticas Detalhadas</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<link rel="stylesheet" href="assets/css/styles.css" />
		<link rel="icon" href="./images/Nowabrand.png">
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body class="no-sidebar">
		<div id="page-wrapper">

			<!-- Header -->
				<div id="header">

					<!-- Inner -->
						<div class="inner">
							<header>
								<h1><a href="index.php" id="logo">NowaTeam</a></h1>
							</header>
						</div>

					<!-- Nav -->
					<?php include 'navigationMenu.php'; ?>

				</div>

			<!-- Main -->
				<div class="wrapper style1">

					<div class="container">
						<article id="main" class="special">
							<header>
								<h2>Estatísticas  Gerais dos Mágicos</h2>
								<p>
								<?php
									$fieldType = "ratio";
									$fieldOrder = "STA_Goals";
									if(isset($_POST['fieldType']) and isset($_POST['fieldOrder'])){
										$fieldType = $_POST['fieldType'];
										$fieldOrder = $_POST['fieldOrder'];
									}								
								?>
								<form action="statistics.php" method="POST">
									<div id= "statFilterRow" class="row">
										<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<div class="align-left">		
													Ordernar por:	
														<select name="fieldOrder" class="filter" onchange="form.submit()" >
														  <option value="STA_Player" <?php if(isset($fieldOrder)){if ($fieldOrder == "STA_Player") {echo "selected";}} ?>>Mágico</option>
														  <option value="STA_Battles" <?php if(isset($fieldOrder)){if ($fieldOrder == "STA_Battles") {echo "selected";}} ?>>Batalhas</option>  
														  <option value="STA_Goals" <?php if(isset($fieldOrder)){if ($fieldOrder == "STA_Goals") {echo "selected";}} else {echo "selected";} ?>>Golos</option>
														  <option value="STA_Assists" <?php if(isset($fieldOrder)){if ($fieldOrder == "STA_Assists") {echo "selected";}} ?>>Assistências</option>
														  <option value="STA_Victory" <?php if(isset($fieldOrder)){if ($fieldOrder == "STA_Victory") {echo "selected";}} ?>>Vitórias</option>
														  <option value="STA_OwnGoals" <?php if(isset($fieldOrder)){if ($fieldOrder == "STA_OwnGoals") {echo "selected";}} ?>>Traições</option>
														  <option value="STA_Pontuality" <?php if(isset($fieldOrder)){if ($fieldOrder == "STA_Pontuality") {echo "selected";}} ?>>Assíduidade</option>  
														  </select>	
												</div>
										</div>
										<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<div class="align-left">Valor de Ordenação:
													<select name="fieldType" class="filter" onchange="form.submit()">
														<option value="ratio" <?php if(isset($fieldType)){if ($fieldType == "ratio") {echo "selected";}} else{echo "selected";} ?>>Rácio</option>
														<option value="absolute" <?php if(isset($fieldType)){if ($fieldType == "absolute") {echo "selected";}} ?>>Absoluto</option>
													</select>	
												</div>
										</div>
									</div>
									</form>
								</p>
							</header>
							<p>
							<?php include ('listStatistics.php'); ?>
							</p>							
						</article>
					</div>

				</div>

			<!-- Footer -->
			<?php include 'footerInclude.php'; ?>
		
		</div>

		<?php include_once('./assets/includes/utils/incScripts.php'); ?>

	</body>
</html>