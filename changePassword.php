<?php
	session_start();
	if (@$_SESSION['auth'] != "yes") {
		header("Location: login.php");
		exit();
	}			
?>
<!DOCTYPE HTML>
<!--
	Helios by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>NowaTeam Utilizadores</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<link rel="icon" href="./images/Nowabrand.png">
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body class="no-sidebar">
		<div id="page-wrapper">

			<!-- Header -->
				<div id="header">

					<!-- Inner -->
						<div class="inner">
							<header>
								<h1><a href="index.php" id="logo">NowaTeam</a></h1>
							</header>
						</div>

					<!-- Nav -->
					<?php include 'navigationMenu.php'; ?>

				</div>

			<!-- Main -->
				<div class="wrapper style1">

					<div class="container">
						<article id="main" class="special">
							<header>
								<h2><a href="#">Alterar a Palavra Passe para <?php 
									$userUsername = @$_SESSION['user'];
									if (isset($_POST['userUsername'])) {$userUsername = $_POST['userUsername'];}
									echo $userUsername; 
								?></a></h2>
							</header>
							<p>
								<?php include ('performChangePassword.php'); ?>
								
								<form action="changePassword.php" method="POST">
									<fieldset>
									
										<label for="userUsername">Utilizador:</label>
										<input type="text" id="userUsername" name="userUsername" <?php echo "value = \"$userUsername\""; ?> readonly>
										
										<br>
										<label for="userPassword">Palavra Passe:</label>
										<input type="password" id="userPassword" required name="userPassword" >
										<br>
										
										<label for="userPasswordRepeat">Repetir Palavra Passe:</label>
										<input type="password" id="userPasswordRepeat" required name="userPasswordRepeat">
										<br>										
										
									<input type="submit" name="btnAction" value="Alterar">
									</fieldset>
									<br>
								</form>
							</p>							
						</article>
					</div>

				</div>

			<!-- Footer -->
			<?php include 'footerInclude.php'; ?>
		
		</div>

		<?php include_once('./assets/includes/utils/incScripts.php'); ?>

	</body>
</html>