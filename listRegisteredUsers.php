<?php
	/*
	include('./assets/misc/misc.inc');
	
	$connection = new PDO("mysql:host=$host;dbname=$database;charset=utf8",$user,$password);
	$connection->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES 'UTF8'");
	*/

	$userGroup = 1000;		
	if (@$_SESSION['group'] > 0) {
		$userGroup = @$_SESSION['group'];	
	}	
	
	include_once("functions.php");
	$connection = getDatabaseConnection();	
	$query = $connection->query("select USE_USERNAME AS 'Manager', USE_Active as 'Ativo', USE_Group as 'Grupo', PLY_name as 'Mágico' from tblUser left join tblPlayer ON USE_Player = PLY_Id WHERE USE_Group <= $userGroup ORDER BY USE_USERNAME ASC");
	
		
	if (! $query) {
		$errorMessage = $connection->errorInfo();
		writeErrorLog("listRegisteredUsers.php - ".$errorMessage);	
		//print_r("Erro de Execução.<br>\n");
	}
	else {
		$column_count = $query->columnCount();
		$result = $query->fetchALL();
		
		echo "<table class=\"default\" style=\"width:100%\">\n";
		echo "\t<tr>\n";
		for ($counter = 0; $counter < $column_count; $counter ++) {
			$meta = $query->getColumnMeta($counter);
			$column_name = $meta['name'];
			echo "\t\t<th>$column_name</th>\n";
		}
		echo "\t\t<th>Acções</th>\n";
		echo "\t</tr>";
		
		echo "\t<tr>\n";
		
		foreach ($result as $row){
			for ($counter = 0; $counter < $column_count; $counter ++) {
					echo "\t\t<td>".utf8_encode($row[$counter])."</td>\n";
			}
			echo "\t\t<td>\n";
			echo "\t\t<form action=\"editRegisteredUser.php\" method=\"POST\" class='operations' >\n";
			echo "\t\t\t<fieldset>\n";
			$useUsername = $row[0];
			echo "\t\t\t<input type=\"hidden\" name=\"useUsername\" id=\"useUsername\" value=\"$useUsername\">\n";
			echo "\t\t\t</fieldset>\n";
			echo "\t\t<button class=\"small\"><img src=\"images/edit_icon32T.png\" /></button>\n";
			echo "\t\t</form>\n";
			/* Reset Password */
			if (@$_SESSION['group'] >= 5) {
				echo "\t\t<form action=\"changePassword.php\" method=\"POST\" class='operations' >\n";
				echo "\t\t\t<fieldset>\n";
				echo "\t\t\t<input type=\"hidden\" name=\"userUsername\" id=\"userUsername\" value=\"$useUsername\">\n";
				echo "\t\t\t</fieldset>\n";
				echo "\t\t<button class=\"small\"><img src=\"images/forgot_pass32.png\" /></button>\n";
				echo "\t\t</form>\n";
				/* Delete USER */
				echo "\t\t<form action=\"deleteRecord.php\" method=\"POST\" class='operations' >\n";
				echo "\t\t\t<fieldset>\n";
				echo "\t\t\t<input type=\"hidden\" name=\"userUsername\" id=\"userUsername\" value=\"$useUsername\">\n";
				echo "\t\t\t<input type=\"hidden\" name=\"originPage\" id=\"originPage\" value=\"viewEditUsers.php\">\n";
				echo "\t\t\t</fieldset>\n";
				echo "\t\t<button class=\"small\" onClick=\"javascript:return confirm('Quer mesmo apagar o Manager?');\" ><img src=\"images/remove_icon32.png\" /></button>\n";
				echo "\t\t</form>\n";
			}
			echo "\t\t</td>\n";
			echo "\t</tr>\n";			
		}
		echo "</table>\n";
	}
?>