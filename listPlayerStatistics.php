<?php
/*
	include('./assets/misc/misc.inc');
	
	$connection = new PDO("mysql:host=$host;dbname=$database;charset=utf8",$user,$password);
	$connection->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES 'UTF8'");
	*/
	include_once("functions.php");
	$connection = getDatabaseConnection();
	$query = $connection->query("SELECT PLY_Name FROM tblPlayer INNER JOIN tblStats ON STA_Player = PLY_ID WHERE STA_GameDate = '$statsDate' ORDER BY PLY_Name");

	if (! $query) {
		echo "<br><p class=\"redInformation\"> Erro de Execução:</p><br>\n";
		$errorMessage = $connection->errorInfo();
		writeErrorLog($errorMessage);		
	}
	else {
		$result = $query->fetchALL();
		
		foreach ($result as $row)
		{
			$playerName = utf8_encode($row['PLY_Name']);			
			echo "- $playerName <br>";
		}
	}
?>