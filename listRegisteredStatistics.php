<?php
	/*
	include('./assets/misc/misc.inc');
	
	$connection = new PDO("mysql:host=$host;dbname=$database;charset=utf8",$user,$password);
	$connection->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES 'UTF8'");
	*/
	include_once("functions.php");
	$connection = getDatabaseConnection();	
	$query = $connection->query("SELECT STA_Player as 'Codigo', STA_GameDate as 'Data', PLY_Name as 'Mágico', STA_Goals as 'Golos', 
												STA_Assists as 'Assistências', STA_OwnGoals as 'Traições', STA_Victory as 'Vitória', 
												STA_Color as 'Equipa', STA_Payed as 'Pago', STA_Obs as 'Observações' from tblStats
												inner join tblPlayer on STA_Player = PLY_id
												order by STA_GameDate DESC, STA_Color DESC, PLY_Name ASC");
		
	if (! $query) {
		echo "Erro de Execução.<br>\n";
		$errorMessage = $connection->errorInfo();
		writeErrorLog("listRegisteredStatistics.php - ".$errorMessage);				
	}
	else {
		$column_count = $query->columnCount();
		$result = $query->fetchALL();
		
		echo "<table class=\"default\" style=\"width:100%\">\n";
		echo "\t<tr>\n";
		for ($counter = 1; $counter < $column_count; $counter ++) {
			$meta = $query->getColumnMeta($counter);
			$column_name = $meta['name'];
			echo "\t\t<th>$column_name</th>\n";
		}
		echo "\t\t<th>Acções</th>\n";
		echo "\t</tr>";
		
		echo "\t<tr>\n";
		
		foreach ($result as $row){
			for ($counter = 1; $counter < $column_count; $counter ++) {
					echo "\t\t<td>".utf8_encode($row[$counter])."</td>\n";
			}
			echo "\t\t<td>\n\t\t<form action=\"editRegisteredStatistics.php\" method=\"POST\" class='operations' >\n";
			echo "\t\t\t<fieldset>\n";
			$staPlayer = $row[0];
			$staDate = $row[1];
			$staPlayerName = utf8_encode($row[2]);
			echo "\t\t\t<input type=\"hidden\" name=\"statsPlayer\" id=\"staPlayer\" value=\"$staPlayer\">\n";
			echo "\t\t\t<input type=\"hidden\" name=\"statsDate\" id=\"staDate\" value=\"$staDate\">\n";
			echo "\t\t\t<input type=\"hidden\" name=\"staPlayerName\" id=\"staPlayerName\" value=\"$staPlayerName\">\n";
			echo "\t\t\t</fieldset>\n";
			echo "\t\t<button class=\"small\"><img src=\"images/edit_icon32T.png\" /></button>\n";
			echo "\t\t</form>\n";
			if (@$_SESSION['group'] >= 5) {
				/* Delete Statistic */
				echo "\t\t<form action=\"deleteRecord.php\" method=\"POST\" class='operations' >\n";
				echo "\t\t\t<fieldset>\n";
				echo "\t\t\t<input type=\"hidden\" name=\"statsPlayer\" id=\"statsPlayer\" value=\"$staPlayer\">\n";
				echo "\t\t\t<input type=\"hidden\" name=\"statsDate\" id=\"statsDate\" value=\"$staDate\">\n";
				echo "\t\t\t<input type=\"hidden\" name=\"originPage\" id=\"originPage\" value=\"viewEditStatistics.php\">\n";
				echo "\t\t\t</fieldset>\n";
				echo "\t\t<button class=\"small\" onClick=\"javascript:return confirm('Quer mesmo apagar a Estatística?');\" ><img src=\"images/remove_icon32.png\" /></button>\n";
				echo "\t\t</form>\n";
			}	
			echo "\t\t</td>\n";
			echo "\t</tr>\n";
		}
		echo "</table>\n";
	}
?>