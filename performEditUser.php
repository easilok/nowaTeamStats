<?php
	
	include_once('functions.php');
	
	$editUsername = $_POST['userUsername'];
	$editType = $_POST['hasPlayer'];
	$editAction = $_POST['btnAction'];
	
	if ($editAction == "Alterar Palavra-Passe") {header('Location: changePassword.php');}
	
	if ($editType) {
		$editPlayerName = $_POST['playerName'];
		$editPlayerEmail = $_POST['playerEmail'];
		$editPlayerPhone = $_POST['playerPhone'];
		$editPlayerQuote = $_POST['playerQuote'];
		$editPlayerObs = $_POST['playerObs'];
		$editPlayerId = $_POST['playerId'];	
		if (strlen($editPlayerName) > 1) {
			$tempPlayerName = $editPlayerName;
		}
		else {
			$tempPlayerName = "na";
		}
		$editPlayerPhoto =  loadUserPhoto($editType, $_POST['playerPhoto'], $editPlayerId, $tempPlayerName);//$_POST['playerPhoto'];
		
		$result = array(
			"USE_USERNAME" => utf8_decode($editUsername),
			"PLY_Name" => utf8_decode($editPlayerName),
			"PLY_Email" => utf8_decode($editPlayerEmail),
			"PLY_Phone" => $editPlayerPhone,
			"PLY_Quote" => utf8_decode($editPlayerQuote),
			"PLY_Id" => $editPlayerId,
			"PLY_Obs" => utf8_decode($editPlayerObs),
			"PLY_Photo" => $editPlayerPhoto
		);
	}
	else {
		$result = array(
			"USE_USERNAME" => utf8_decode($editUsername)
		);			
	}
	
	if ($editAction == "Editar"){			
			//listUserDetails(true, $result , $editType, true);

			}
	elseif ($editAction == "Guardar"){			
			saveUserDetails($result , $editType);
					header('Location: viewUser.php');
	}

?>