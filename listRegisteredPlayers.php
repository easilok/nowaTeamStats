<?php
	/*
	include('./assets/misc/misc.inc');
	
	$connection = new PDO("mysql:host=$host;dbname=$database;charset=utf8",$user,$password);
	$connection->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES 'UTF8'");
	*/
	
	include_once("functions.php");
	$connection = getDatabaseConnection();
	$query = $connection->query("select PLY_ID as 'Registo', PLY_Name AS 'Mágico', PLY_Email as 'EMAIL', PLY_Phone as 'Telemóvel', PLY_Quote as 'Citação', PLY_Obs as 'Observação' FROM tblPlayer ORDER BY PLY_Name ASC");

		
	if (! $query) {
		echo "<br><p class=\"redInformation\"> Erro de Execução:</p><br>\n";
		$errorMessage = $connection->errorInfo();
		writeErrorLog($errorMessage);	
	}
	else {
		$column_count = $query->columnCount();
		$result = $query->fetchALL();
		
		echo "<table class=\"default\" style=\"width:100%\">\n";
		echo "\t<tr>\n";
		for ($counter = 1; $counter < $column_count; $counter ++) {
			$meta = $query->getColumnMeta($counter);
			$column_name = $meta['name'];
			echo "\t\t<th>$column_name</th>\n";
		}
		echo "\t\t<th>Acções</th>\n";
		echo "\t</tr>";
		
		echo "\t<tr>\n";
		
		foreach ($result as $row){
			for ($counter = 1; $counter < $column_count; $counter ++) {
					echo "\t\t<td>".utf8_encode($row[$counter])."</td>\n";
			}
			echo "\t\t<td>\n\t\t<form action=\"editRegisteredPlayers.php\" method=\"POST\" class='operations'>\n";
			echo "\t\t\t<fieldset>\n";
			$playerId = $row[0];
			$playerName = utf8_encode($row[1]);
			echo "\t\t\t<input type=\"hidden\" name=\"playerId\" id=\"playerId\" value=\"$playerId\">\n";
			echo "\t\t\t<input type=\"hidden\" name=\"playerName\" id=\"playerName\" value=\"$playerName\">\n";
			echo "\t\t\t</fieldset>\n";
			echo "\t\t<button class=\"small\"><img src=\"images/edit_icon32T.png\" /></button>\n";
			echo "\t\t</form>\n";
			if (@$_SESSION['group'] >= 5) {
				/* Delete Player */
				echo "\t\t<form action=\"deleteRecord.php\" method=\"POST\" class='operations' >\n";
				echo "\t\t\t<fieldset>\n";
				echo "\t\t\t<input type=\"hidden\" name=\"playerId\" id=\"playerId\" value=\"$playerId\">\n";
				echo "\t\t\t<input type=\"hidden\" name=\"originPage\" id=\"originPage\" value=\"viewEditPlayers.php\">\n";
				echo "\t\t\t</fieldset>\n";
				echo "\t\t<button class=\"small\" onClick=\"javascript:return confirm('Quer mesmo apagar o Mágico?');\" ><img src=\"images/remove_icon32.png\" /></button>\n";
				echo "\t\t</form>\n";
			}	
			echo "\t\t</td>\n";
			echo "\t</tr>\n";			
		}
		echo "</table>\n";
	}
?>