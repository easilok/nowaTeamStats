<?php

	$orderRatio = array(		
		"STA_Player" => "STA_Player ASC",
		"STA_OldPosition" => "Coalesce(oldQuery.oldPosition,1000) ASC",
		"STA_Battles" => "COUNT(STA_GameDate) DESC",
		"STA_Goals" => "ROUND(SUM(STA_Goals)/COUNT(STA_GameDate),2) DESC",
		"STA_Assists" => "ROUND(SUM(STA_Assists)/COUNT(STA_GameDate),2) DESC",
		"STA_Victory" => "ROUND(SUM(STA_Victory = 1)/COUNT(STA_GameDate),2) DESC",
		"STA_OwnGoals" => "ROUND(SUM(STA_OwnGoals)/COUNT(STA_GameDate),2) DESC",
		"STA_Pontuality" => "ROUND(100 * (COUNT(STA_GameDate)/TotalJogos.Jogos),0) DESC"
	);
	
	$orderAbsolute = array(		
		"STA_Player" => "STA_Player ASC",
		"STA_OldPosition" => "Coalesce(oldQuery.oldPosition,1000) ASC",
		"STA_Battles" => "COUNT(STA_GameDate) DESC",
		"STA_Goals" => "SUM(STA_Goals) DESC",
		"STA_Assists" => "SUM(STA_Assists) DESC",
		"STA_Victory" => "SUM(STA_Victory = 1) DESC",
		"STA_OwnGoals" => "SUM(STA_OwnGoals) DESC",
		"STA_Pontuality" => "ROUND(100 * (COUNT(STA_GameDate)/TotalJogos.Jogos),0) DESC"
	);
	
	$fieldType = "ratio";
	$fieldOrder = "STA_Goals";
	if(isset($_POST['fieldType']) and isset($_POST['fieldOrder'])){
		$fieldType = $_POST['fieldType'];
		$fieldOrder = $_POST['fieldOrder'];
	}
	
	if ($fieldType== "absolute"){
		$strWhere = $orderAbsolute[$fieldOrder];
	}
	else {
		$strWhere = $orderRatio[$fieldOrder];	
	}
	
	include ('functions.php');
	
	getStatsTable(0,$strWhere);
	
?>