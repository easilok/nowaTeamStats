<?php
	session_start();
	if ((@$_SESSION['auth'] != "yes") or (@$_SESSION['group'] < 4)){
		header("Location: index.php");
		exit();
	}		
?>
<!DOCTYPE HTML>
<!--
	Helios by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>NowaTeam: Editar Estatística</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<link rel="icon" href="./images/Nowabrand.png">
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body class="no-sidebar">
		<div id="page-wrapper">

			<!-- Header -->
				<div id="header">

					<!-- Inner -->
						<div class="inner">
							<header>
								<h1><a href="index.php" id="logo">NowaTeam</a></h1>
							</header>
						</div>

					<!-- Nav -->
					<?php include 'navigationMenu.php'; ?>

				</div>

			<!-- Main -->
				<div class="wrapper style1">

					<div class="container">
						<article id="main" class="special">
							<header>
								<h2><a href="#">Editar Estatistica para o Mágico <?php echo $_POST['staPlayerName']." na data ".$_POST['statsDate']."<br>"; ?></a></h2>
							</header>
							<p>
								<?php
									include('functions.php');
									
									if (isset($_POST['btnAction'])){if ($_POST['btnAction'] == "Guardar") 
									{
										$staPlayer = $_POST['statsPlayer'];
										$staDate = $_POST['statsDate'];
										$staGoals = $_POST['statsGoals'];
										$staAssists = $_POST['statsAssists'];
										$staOwnGoals = $_POST['statsOwnGoals'];
										$staVictory = false;
										if (isset($_POST['statsVictory'])){
											if ($_POST['statsVictory'] == 1){
												$staVictory = true;
											}
										}
										//$staVictory = $_POST['statsVictory'];
										$staColor = $_POST['statsColor'];
										$staPayed = false;
										if (isset($_POST['statsPayed'])){
											if ($_POST['statsPayed'] == 1){
												$staPayed = true;
											}
										}										
										//$staPayed = $_POST['statsPayed'];
										$staObs = $_POST['statsObs'];
										
										$result = array(
											"STA_Player" => $staPlayer,
											"STA_GameDate" => $staDate,
											"STA_Goals" => $staGoals,
											"STA_Assists" => $staAssists,
											"STA_OwnGoals" => $staOwnGoals,
											"STA_Victory" => $staVictory,
											"STA_Color" => $staColor,
											"STA_Payed" => $staPayed,
											"STA_Obs" => utf8_decode($staObs)
										);
										saveStatisticDetails($result);
									}}
									else{
										$row = getStatsInfo($_POST['statsPlayer'],$_POST['statsDate']);
										$staPlayer = $row['STA_Player'];
										$staDate = $row['STA_GameDate'];
										$staGoals = $row['STA_Goals'];
										$staAssists = $row['STA_Assists'];
										$staOwnGoals = $row['STA_OwnGoals'];
										$staVictory = $row['STA_Victory'];
										$staColor = $row['STA_Color'];
										$staPayed = $row['STA_Payed'];
										$staObs = utf8_encode($row['STA_Obs']);
										
										$result = array(
											"STA_Player" => $staPlayer,
											"STA_GameDate" => $staDate,
											"STA_Goals" => $staGoals,
											"STA_Assists" => $staAssists,
											"STA_OwnGoals" => $staOwnGoals,
											"STA_Victory" => $staVictory,
											"STA_Color" => $staColor,
											"STA_Payed" => $staPayed,
											"STA_Obs" => utf8_decode($staObs)
										);
										
									}
								?>
								<form action="editRegisteredStatistics.php" method="POST">
									<fieldset>
									
										<?php listStatsDetails(true, $result); ?>
										
									</fieldset>
									<br>
								</form>
							</p>							
						</article>
					</div>

				</div>

			<!-- Footer -->
			<?php include 'footerInclude.php'; ?>
		
		</div>

		<?php include_once('./assets/includes/utils/incScripts.php'); ?>

	</body>
</html>