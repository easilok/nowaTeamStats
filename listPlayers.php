<?php
	/*
	include('./assets/misc/misc.inc');

	$connection = new PDO("mysql:host=$host;dbname=$database;charset=utf8",$user,$password);
	$connection->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES 'UTF8'");
	*/
	include_once("functions.php");
	$connection = getDatabaseConnection();
	$query = $connection->query("Select PLY_ID, PLY_Name from tblPlayer WHERE PLY_id NOT IN (SELECT STA_Player FROM tblStats WHERE STA_GameDate = '$statsDate') ORDER BY PLY_Name");
	
	if (! $query) {
		echo "<br><p class=\"redInformation\"> Erro de Execução:</p><br>\n";
		$errorMessage = $connection->errorInfo();
		writeErrorLog($errorMessage);				
	}
	else {
		$result = $query->fetchALL();
		
		echo "<select class=\"default\" name=\"statsPlayer\">";
		foreach ($result as $row)
		{
			$playerID =$row['PLY_ID'];
			$playerName = utf8_encode($row['PLY_Name']);			
			echo "<option value=\"$playerID\">$playerName</option>";
		}
		echo "</select>";
	}
?>