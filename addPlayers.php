﻿<?php
	
	session_start();

	if ((@$_SESSION['auth'] != "yes") or (@$_SESSION['group'] < 2)){
		header("Location: index.php");
		exit();
	}	
	
?>
<!DOCTYPE HTML>
<!--
	Helios by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>NowaTeam Adicionar Novo Jogador</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<link rel="icon" href="./images/Nowabrand.png">
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<?php 	header('Content-Type: text/html; charset=utf-8'); ?>
	</head>
	<body class="no-sidebar">
	
		<div id="page-wrapper">

			<!-- Header -->
				<div id="header">

					<!-- Inner -->
						<div class="inner">
							<header>
								<h1><a href="index.php" id="logo">NowaTeam</a></h1>
							</header>
						</div>

					<!-- Nav -->
					<?php include 'navigationMenu.php'; ?>

				</div>

			<!-- Main -->
				<div class="wrapper style1">

					<div class="container">
						<article id="main" class="special">
							<header>
								<h2><a href="#">Adicionar Novo Jogador</a></h2>
							</header>
							<p>
							
							<?php include 'addPlayer2Database.php'; ?>
							
								<form action="addPlayers.php" method="POST">
									<fieldset>
										
											<br>
										
											<label for="playerName">Nome:</label>
											<input type="Text" id="playerName" required name="playerName" <?php if (isset($playerName)) {echo "value = \"$playerName\"";} ?>>
											
											<br>
											
											<label for="playerEmail">Email:</label>
											<input type="email" id="playerEmail" name="playerEmail" <?php if (isset($playerEmail)) {echo "value = $playerEmail";} ?> >

											<br>
											
											<label for="playerPhone">Telemóvel:</label>
											<input type="tel" id="playerPhone" maxlength ="9" name="playerPhone" <?php if (isset($playerPhone)) {echo "value = $playerPhone";} ?> >
											
											<br>
											
											<label for="playerQuote">Citação:</label>
											<input type="Text" id="playerQuote" name="playerQuote" <?php 
											if (isset($playerQuote)) {
												if ($playerQuote == "") {$playerQuote = "Sem Imaginação";}
											}  
											else {$playerQuote = "Sem Imaginação";}

											echo "value = \"$playerQuote\""; 

											?> > 
											
											<br>											
											<!--<input type="submit" value="Adicionar">-->
									<input type="submit" name="btnAction" value="Adicionar">
									</fieldset>
									<br>
								</form>
							</p>							
						</article>
					</div>

				</div>

			<!-- Footer -->
			<?php include 'footerInclude.php'; ?>
		
		</div>

		<?php include_once('./assets/includes/utils/incScripts.php'); ?>

	</body>
</html>
