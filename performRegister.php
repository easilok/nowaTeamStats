<?php
		//include('./assets/misc/misc.inc');
		
		if (isset($_POST['username'])){
			
			$username = $_POST['username'];
			$userPassword = $_POST['userPassword'];
			$userPasswordRepeat = $_POST['userPasswordRepeat'];
			$userObs = $_POST['userObs'];
			$userGroup = 0;
			$userPlayer = null;
			if (isset($_POST['userGroup'])){
				$userGroup = $_POST['userGroup'];
			}
			if (isset($_POST['userPlayer'])){
				$userPlayer = $_POST['userPlayer'];
				if ($userPlayer <= 0) {
						$userPlayer = null;
				}
			}			

			if (strlen($userPassword) < 6) {
				echo "<br><p class=\"redInformation\"> A palavra passe deve conter mais de 6 caracteres</p><br>";	
			} elseif (preg_match('/^[A-Za-z][A-Za-z0-9]{3,19}$/', $username) == 0) {
			    echo "<br><p class=\"redInformation\">Utilizador não válido. Utilize apenas Letras e números (Começando por Letras). Minimo 4 e máximo 29 caracteres.</p><br>";	
			}
			else {
				if ($userPassword == $userPasswordRepeat) {
					
					/*
					$connection = new PDO("mysql:host=$host;dbname=$database;charset=utf8",$user,$password);
					$connection->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES 'UTF8'");
					*/
					include_once("functions.php");
					$connection = getDatabaseConnection();
					$query = $connection->query("Select COUNT(USE_id) from tblUser where USE_USERNAME = '$username'");
				
					if (! $query) {
						echo "<br><p class=\"redInformation\"> Erro de Execução:</p><br>\n";
						$errorMessage = $connection->errorInfo();
						writeErrorLog($errorMessage);	
					}
					else {
						$count = $query->fetchColumn();
						
						/* Utilizador existente*/
						if ($count > 0 ) {
							echo "<br><p class=\"redInformation\"> Utilizador já existe. </p><br>";
						}
						else {
							$query = $connection->prepare("INSERT INTO tblUser (USE_USERNAME,USE_Pass,USE_RegisterDate,USE_Group,USE_Active,USE_PassExpired, USE_DateModify, USE_Player, USE_OBS) VALUES (:username,:userPassword,:userRegisterDate,:userGroup,:userActive,:userPassExpired,:userDateModify,:userPlayer,:userObs)");

							/* SE criado por outro utilizador, força reset de palavra pass */
							if ((@$_SESSION['auth'] == "yes") and (@$_SESSION['group'] > 4)){ 
								$userActive = 1;
								$userPassExpired = 1;
							}
							else {
								$userPassExpired = 0;
								$userActive = 0;
							}
							
							$numRows = $query->execute(array(
								"username" => utf8_decode($username),
								"userPassword" => hash('sha512',$userPassword),
								"userRegisterDate" => date("Y-m-d"),
								"userGroup" => $userGroup,
								"userActive" => $userActive,
								"userPassExpired" => $userPassExpired,
								"userDateModify" => date("Y-m-d"),
								"userPlayer" => $userPlayer,
								"userObs" => $userObs,
							));	
							
							if (!$numRows) {
								echo "<br><p class=\"redInformation\"> Erro ao Inserir Utilizador: $username </p><br>";
								$errorMessage = $query->errorInfo();
								writeErrorLog($errorMessage);	
							}
							else {
								echo "<br><p class=\"redInformation\"> Inserido Utilizador: $username </p><br>";

								$usernameLogged = @$_SESSION['user'];

								if ($usernameLogged <> '') {
									writeDataBaseLog($usernameLogged, 'Inserir', 'Inserido Utilizador: '.$username, 1);
								}
								
								if ($userActive == 0) {
									//User Registered
									//Send Email
									include_once($BASE_DIR.'/assets/misc/misc_contact.inc');
									$to = $adminEmail;

									// subject
									$subject = 'NowateamStatistics Email Service';

									// message
									$message = '';
									$message .= '<html><body>';
									$message .= '<h1>NowateamStatistics Registo de Utilizador!</h1><br><br>';
									$message .= 'Foi registado um novo utilizador: '.$username.'.<br><br>';
									$message .= 'Efetue a sua activação assim que possível.<br><br>';
									$message .= 'Observações do Registo: '.$userObs.' <br><br>';
									//$message .= '<a href="http://nowateamstats.net16.net/login.php">NowateamStatistics</a>';
									$message .= '</body></html>';

									// headers
									$headers = "From: NowateamStatistics <no-reply@nowateamstats.net16.net>\r\n";
									$headers .= "MIME-Version: 1.0\r\n";
									$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
									// Mail it
									mail($to, $subject, $message, $headers);
								}

								$username = "";
							}
						}
					}
				}
				else{
					echo "<br><p class=\"redInformation\"> Palavras Passe não coincidem.</p><br>";
				}
			}
		}
?>