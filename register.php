<?php
	session_start();
?>
<!DOCTYPE HTML>
<!--
	Helios by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>NowaTeam Registo</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<link rel="icon" href="./images/Nowabrand.png">
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body class="no-sidebar">
		<div id="page-wrapper">

			<!-- Header -->
				<div id="header">

					<!-- Inner -->
						<div class="inner">
							<header>
								<h1><a href="index.php" id="logo">NowaTeam</a></h1>
							</header>
						</div>

					<!-- Nav -->
					<?php include 'navigationMenu.php'; ?>

				</div>

			<!-- Main -->
				<div class="wrapper style1">

					<div class="container">
						<article id="main" class="special">
							<header>
								<h2><a href="#">Registar um novo Manager</a></h2>
							</header>
							<p>
								<?php include ('performRegister.php'); ?>
								
								<form action="register.php" method="POST">
									<fieldset>
									
										<label for="username">Utilizador:</label>
										<input type="text" id="username" required name="username" <?php if(isset($username)) {echo "value = \"$username\"";} ?> >
										
										<br>
										<label for="userPassword">Palavra Passe:</label>
										<input type="password" id="userPassword" required name="userPassword" >
										<br>
										
										<label for="userPasswordRepeat">Repetir Palavra Passe:</label>
										<input type="password" id="userPasswordRepeat" required name="userPasswordRepeat">
										<br>										
										
										<?php
											/* Se tem login feito, e abriu a página é porque tem previlégios de selecionar caraterísticas*/
											if  ((@$_SESSION['auth'] == 'yes' ) and (@$_SESSION['group'] > 4)) {
												/* Grupo do utilizador */
												echo "<label for=\"userGroup\">Grupo do Utilizador:</label>\n";
												echo "<select class=\"default\" name=\"userGroup\">\n";
												for ($counter = 0; $counter <= 5; $counter++)
												{
													echo "<option value=\"$counter\"";
													if (isset($userGroup)) {if ($userGroup == $counter) {echo " selected ";}}
													echo ">$counter</option>\n";
												}
												echo "</select>\n";
												echo "<br>\n";
												
												include('functions.php');
												
												/* Grupo do utilizador */
												echo "<label for=\"userPlayer\">Mágico:</label>\n";
												$result = getPlayersNoUser();
												
												echo "<select class=\"default\" name=\"userPlayer\">\n";
												echo "<option value=\"0\">Sem Mágico</option>\n";
												foreach ($result as $row)
												{
													$tempPlayerID = $row['PLY_ID'];
													$tempPlayerName =  utf8_encode($row['PLY_Name']);
													echo "<option value=\"$tempPlayerID\"";
													if (isset($userGroup)) {if ($userPlayer == $tempPlayerID) {echo " selected ";}}
													echo ">$tempPlayerName</option>\n";
												}
												echo "</select>\n";
												echo "<br>\n";												
											}
										?>

										<label for="userObs">Observações:</label>
										<input type="text" id="userObs" name="userObs">
										<br>			

									<input type="submit" name="btnAction" value="Adicionar">
									</fieldset>
									<br>
								</form>
							</p>							
						</article>
					</div>

				</div>

			<!-- Footer -->
			<?php include 'footerInclude.php'; ?>
		
		</div>

		<?php include_once('./assets/includes/utils/incScripts.php'); ?>

	</body>
</html>