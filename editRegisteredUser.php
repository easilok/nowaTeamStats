<?php
	session_start();
	if ((@$_SESSION['auth'] != "yes") or (@$_SESSION['group'] < 5)){
		header("Location: index.php");
		exit();
	}		
?>
<!DOCTYPE HTML>
<!--
	Helios by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>NowaTeam: Editar Manager</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<link rel="icon" href="./images/Nowabrand.png">
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body class="no-sidebar">
		<div id="page-wrapper">

			<!-- Header -->
				<div id="header">

					<!-- Inner -->
						<div class="inner">
							<header>
								<h1><a href="index.php" id="logo">NowaTeam</a></h1>
							</header>
						</div>

					<!-- Nav -->
					<?php include 'navigationMenu.php'; ?>

				</div>

			<!-- Main -->
				<div class="wrapper style1">

					<div class="container">
						<article id="main" class="special">
							<header>
								<h2><a href="#">Editar o Manager <?php 	echo $_POST['useUsername']."<br>"; ?></a></h2>
							</header>
							<p>
								<?php
									include('functions.php');
									
									if (isset($_POST['btnAction'])){if ($_POST['btnAction'] == "Atualizar") 
									{
										$useUsername = ($_POST['useUsername']);
										if ($_POST['useActive'] == 1) {
											$useActive = true;
										}
										else {$useActive = false;}
										$useGroup = $_POST['useGroup'];
										$usePlayer = $_POST['usePlayer'];
										$useId = $_POST['useId'];
										$usePlayerName= $_POST['usePlayerName'];
										
										$result = array(
											"USE_USERNAME" => utf8_decode($useUsername),
											"USE_Active" => $useActive,
											"USE_Group" => $useGroup,
											"USE_Player" => $usePlayer,
											"USE_Id" => $useId
										);
										saveManagerDetails($result);
									}}
									else{
										$row = getUserInfo($_POST['useUsername']);
										$useUsername = utf8_encode($row['USE_USERNAME']);
										$useActive = $row['USE_Active'];
										$useGroup = $row['USE_Group'];
										$usePlayer = $row['USE_Player'];
										$useId = $row['USE_Id'];
										$usePlayerName = utf8_encode($row['PLY_Name']);

									}
								?>
								<form action="editRegisteredUser.php" method="POST">
									<fieldset>
										<label for="useUsername">Utilizador:</label>
										<input type="text" id="useUsername" required readonly name="useUsername" <?php echo "value=\"$useUsername\"";?>>
										<br>
										
										<input type="checkbox" id="useActive" name="useActive" value="1"<?php if($useActive){echo "checked";}?>>
										<label for="useActive"><span></span><strong>Manager Activo:</strong></label>
										
										<label for="useGroup">Manager Grupo:</label>
										<select class="default" name="useGroup">
											<?php
												$maxGroup = @$_SESSION['group'];
												for ($i = 0; $i <= $maxGroup; $i++) {
													echo "<option value=\"$i\"";
													if ($i == $useGroup){echo " selected ";}
													echo ">$i</option>\n";
												}
											?>
										</select>
										<br>										

										<label for="usePlayer">Mágico:</label>
										<select class="default" name="usePlayer">
											<?php
												echo "<option value=\"$usePlayer\" selected >$usePlayerName</option>\n";												
													
												$result = getPlayersNoUser();
												
												foreach ($result as $row)
												{
													$tempPlayerID = $row['PLY_ID'];
													$tempPlayerName =  utf8_encode($row['PLY_Name']);
													echo "<option value=\"$tempPlayerID\">$tempPlayerName</option>\n";
												}			
											?>
										</select>				
										<br>
										<input type="hidden" name="useId" <?php echo "value=\"$useId\"";?>>
										<input type="hidden" name="usePlayerName" <?php echo "value=\"$usePlayerName\"";?>>
										<input type="submit" name="btnAction" value="Atualizar">
									</fieldset>
								</form>
							</p>							
						</article>
					</div>

				</div>

			<!-- Footer -->
			<?php include 'footerInclude.php'; ?>
		
		</div>

		<?php include_once('./assets/includes/utils/incScripts.php'); ?>

	</body>
</html>