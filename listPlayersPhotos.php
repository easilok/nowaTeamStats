<?php
	include_once('functions.php');	
	/*
	include('./assets/misc/misc.inc');
		
	$connection = new PDO("mysql:host=$host;dbname=$database;charset=utf8",$user,$password);
	$connection->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES 'UTF8'");
	*/
	
	$connection = getDatabaseConnection();
	$query = $connection->query("select PLY_Name, PLY_Quote, PLY_Photo, PLY_ID, CONCAT(Coalesce(t1.Assiduidade,0),'%') as Assiduidade from tblPlayer
		left outer join (
		SELECT STA_Player, Coalesce(ROUND(100 * (COUNT(STA_GameDate)/TotalJogos.Jogos),0),0) as 'Assiduidade' from tblStats inner Join (SELECT COUNT(DISTINCT STA_GameDate) as 'Jogos' FROM tblStats) as TotalJogos GROUP BY STA_Player ) as t1
		on STA_Player = PLY_ID
		ORDER BY t1.Assiduidade DESC, PLY_Name ASC");
	
	if (!$query){
		echo "<br><p class=\"redInformation\"> Erro de Execução:</p><br>\n";
		$errorMessage = $connection->errorInfo();
		writeErrorLog($errorMessage);			
		$query = null;
		$connection = null;
	}
	else {
		$result = $query->fetchALL();
		$counter = 0;
		echo "<div class='row'>\n";
		foreach ($result as $row) {
			if ($counter == 3) {
				echo "</div>\n<div class='row'>\n";
				$counter = 0;
			}
			echo "<article class='4u 12u(mobile) special'>\n";
			$playerName = utf8_encode($row['PLY_Name']);
			$playerQuote = utf8_encode($row['PLY_Quote']);
			if (($row['PLY_Photo'] == null) or ($row['PLY_Photo'] == "")){
				$row['PLY_Photo'] = "./magicPhotos/default.jpg";
			}					
			else {
				if (!file_exists($row['PLY_Photo'])) {$row['PLY_Photo'] = "./magicPhotos/default.jpg";}
			}
			$playerPhoto = $row['PLY_Photo'];
			$playerAssiduity = $row['Assiduidade'];
			echo "<img src='$playerPhoto' alt='$playerName' height = '300' width = '100%' />\n";
			echo "<header>\n<h3>$playerName</h3>\n</header>\n";
			echo "<p>$playerQuote\n<br>\nPresenças: $playerAssiduity\n</p>\n";
			echo "</article>\n";
			$counter += 1;
		}
		echo "</div>\n";
	}		
	
?>