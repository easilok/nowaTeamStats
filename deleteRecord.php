<?php		
	include_once('functions.php');
	
	if (isset($_POST['originPage'])){
		$returnPage = $_POST['originPage'];
		
		switch ($returnPage) {
			case "viewEditUsers.php":
				$userUsername = $_POST['userUsername'];
				deleteUser($userUsername);
				break;
			case "viewEditPlayers.php":
				$playerId = $_POST['playerId'];
				$error = deletePlayer($playerId);
				break;	
			case "viewEditStatistics.php":
				$statsPlayer = $_POST['statsPlayer'];
				$statsDate = $_POST['statsDate'];
				$error = deleteStats($statsPlayer, $statsDate);
				break;				
		}
		
		header("Location: $returnPage");
	}
	else {
		header("Location: index.php");
	}
?>