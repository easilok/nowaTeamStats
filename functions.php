<?php
	
	/* General Functions */

	/* GET an object connection to database */

	$BASE_DIR = ".";
	$BASE_DIR = dirname(__FILE__);
	//$initPath = $_SERVER['DOCUMENT_ROOT'];

	function getDatabaseConnection() {
		global $BASE_DIR;
		include($BASE_DIR.'/assets/misc/misc.inc');

		$_retVal = null; 
		
		try {
		
			$connection = new PDO("mysql:host=$host;dbname=$database;charset=utf8",$user,$password);
			$connection->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES 'UTF8'");
			
			$_retVal = $connection;
			
		} catch (PDOException $e) {
			print "Erro de connexão à Base de Dados <br/>";
			writeErrorLog($e->getMessage());
			die();
		}
		
		return $_retVal;
		
	}

	/* Encode and Decode Strings */

	function str2StrSql ($strOriginal) {
		return utf8_decode($strOriginal);
	}

	function StrSql2str($strOriginal) {
		return utf8_encode($strOriginal);
	}
	
	function getUserGroup($useUsername) {
		/*
		include('./assets/misc/misc.inc');
		
		$connection = new PDO("mysql:host=$host;dbname=$database;charset=utf8",$user,$password);
		$connection->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES 'UTF8'");
		*/
		$connection = getDatabaseConnection();

		
		$query = $connection->query("Select USE_Group from tblUser where USE_USERNAME = '$useUsername'");
			
		if (! $query) {
			/*print_r($connection->errorInfo());*/
			$errorMessage = $connection->errorInfo();
			writeErrorLog($errorMessage);					
			$query  = null;
			$connection  = null;
			return 0;
		}
		else {
			$row = $query->fetch();
			
			$userGroup = $row['0'];
			
			$query  = null;
			$connection  = null;
			
			if (is_numeric($userGroup)) {
				return $userGroup;				
			}
			else {
				return 0;
			}
		} 		
	}
	
	function getPlayersNoUser() {
		/*
		include('./assets/misc/misc.inc');
		
		$connection = new PDO("mysql:host=$host;dbname=$database;charset=utf8",$user,$password);
		$connection->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES 'UTF8'");
		*/
		$connection = getDatabaseConnection();
		
		$query = $connection->query("Select PLY_ID, PLY_Name
													from tblPlayer 
													WHERE PLY_ID NOT IN 
													(SELECT Coalesce(USE_Player,0) FROM tblUser) 
													ORDER BY PLY_Name");
			
		if (! $query) {
			//print_r("Erro de Execução.\n");
			$errorMessage = $connection->errorInfo();
			writeErrorLog($errorMessage);				
			$query  = null;
			$connection  = null;
			return "";
		}
		else {
			$result = $query->fetchAll();
			$query  = null;
			$connection  = null;			
			/*
			foreach ($result as $row) {			
				echo $row['PLY_Name']."<br>";
			}
			*/
			return $result;
		} 
	}
	
	/* FUNCOES DE JOGADORES */ 

	function getPlayerName($playerID) {
		/*
		include('./assets/misc/misc.inc');
		
		$connection = new PDO("mysql:host=$host;dbname=$database;charset=utf8",$user,$password);
		$connection->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES 'UTF8'");
		*/
		$connection = getDatabaseConnection();
		$query = $connection->query("Select PLY_Name from tblPlayer
													WHERE PLY_ID='$playerID'");
			
		if (! $query) {
			//print_r("Erro de Execução.\n");
			$errorMessage = $connection->errorInfo();
			writeErrorLog($errorMessage);					
			return -1;
		}
		else {
			$result = utf8_encode($query->fetchColumn());
			return $result;
		} 
	}	
	
	
	function getUserPlayerId($userUsername) {
		/*
		include('./assets/misc/misc.inc');
		
		$connection = new PDO("mysql:host=$host;dbname=$database;charset=utf8",$user,$password);
		$connection->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES 'UTF8'");
		*/
		$connection = getDatabaseConnection();
		$query = $connection->query("Select Coalesce(USE_Player,-1)
													from tblUser
													WHERE USE_USERNAME='$userUsername'");
			
		if (! $query) {
			//print_r("Erro de Execução.\n");
			$errorMessage = $connection->errorInfo();
			writeErrorLog($errorMessage);				
			return -1;
		}
		else {
			$result = $query->fetchColumn();
			/*
			foreach ($result as $row) {			
				echo $row['PLY_Name']."<br>";
			}
			*/
			return $result;
		} 
	}	
	
	function getPlayerListNoStat($statsDate) {
		/*
		include('./assets/misc/misc.inc');
		
		$connection = new PDO("mysql:host=$host;dbname=$database;charset=utf8",$user,$password);
		$connection->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES 'UTF8'");
		*/
		$connection = getDatabaseConnection();
		$query = $connection->query("Select PLY_ID, PLY_Name from tblPlayer WHERE PLY_id NOT IN (SELECT STA_Player FROM tblStats WHERE STA_GameDate = '$statsDate') ORDER BY PLY_Name");
		

		if (! $query) {
			//print_r("Erro de Execução.\n");
			$errorMessage = $connection->errorInfo();
			writeErrorLog($errorMessage);					
			return -1;
			}
		else {
			$result = $query->fetchALL();
			return $result;
		}
	}		
	
	function listUserDetails($editing, $row, $hasPlayer, $isUserView) {
			
			if ($isUserView) {
				$lstUsername = utf8_encode($row['USE_USERNAME']);
			
				echo "<label for=\"userUsername\">Utilizador:</label>";
				echo "<input type=\"text\" id=\"userUsername\" required name=\"userUsername\""; 
				//if (!$editing) {
					echo " readonly ";
				//}
				echo " value=\"$lstUsername\"><br>";
				echo "<input type=\"hidden\" name=\"hasPlayer\" id=\"hasPlayer\" value=\"$hasPlayer\">";
			}
			
			if ($hasPlayer) {

				$lstName = utf8_encode($row['PLY_Name']);
				$lstEmail = utf8_encode($row['PLY_Email']);
				$lstPhone = $row['PLY_Phone'];
				$lstQuote = utf8_encode($row['PLY_Quote']);
				$lstObs = utf8_encode($row['PLY_Obs']);
				$lstId = $row['PLY_Id'];
				$lstPhoto = $row['PLY_Photo'];
			
				echo "<label for=\"playerName\">Nome de Mágico:</label>";
				echo "<input type=\"text\" id=\"playerName\" required name=\"playerName\""; 
				if (!$editing) {
					echo " readonly ";
				}
				echo " value=\"$lstName\"><br>";
				
				echo "<label for=\"playerEmail\">Email:</label>";
				echo "<input type=\"text\" id=\"playerEmail\" name=\"playerEmail\""; 
				if (!$editing) {echo " readonly ";}
				echo " value=\"$lstEmail\"><br>";
				
				echo "<label for=\"playerPhone\">Telemóvel:</label>";
				echo "<input type=\"tel\" id=\"playerPhone\" required name=\"playerPhone\""; 
				if (!$editing) {echo " readonly ";}
				echo " value=\"$lstPhone\"><br>";
				
				echo "<label for=\"playerQuote\">Citação:</label>";
				echo "<input type=\"text\" id=\"playerQuote\" required name=\"playerQuote\""; 
				if (!$editing) {echo " readonly ";}
				echo " value=\"$lstQuote\"><br>";
				
				echo "<label for=\"playerObs\">Observação:</label>";
				echo "<input type=\"text\" id=\"playerObs\" name=\"playerObs\""; 
				if (!$editing) {echo " readonly ";}
				echo " value=\"$lstObs\"><br>";				
				
				echo "<input type=\"hidden\" name=\"playerId\" id=\"playerId\" value=\"$lstId\">";
				echo "<input type=\"hidden\" name=\"playerPhoto\" id=\"playerPhoto\" value=\"$lstPhoto\">";
			}
			
			if (!$editing){
				$horizontalSpace = "5%";
				echo "<div class=\"col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12\">\n";
				echo "<input id=\"btn-edit-user\" class=\"btn-row btn-row-left\" name=\"btnAction\" type=\"submit\" value=\"Editar\">\n";
				echo "</div>\n";

				echo "<div class='clearfix visible-xs-block'></div>\n";
				
				echo "<div class=\"col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12\">\n";
				echo "<input id=\"btn-change-password\" class=\"btn-row btn-row-right\" name=\"btnAction\" type=\"submit\" value=\"Alterar Palavra-Passe\">\n";
				echo "</div>\n";				
			}
			else {
				echo "<input name=\"btnAction\" type=\"submit\" value=\"Guardar\">";	
			}
	}		
	
	function getMagicInfo($playerID) {
		/*
		include('./assets/misc/misc.inc');
		
		$connection = new PDO("mysql:host=$host;dbname=$database;charset=utf8",$user,$password);
		$connection->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES 'UTF8'");
		*/
		global $BASE_DIR;

		$connection = getDatabaseConnection();		
		$query = $connection->query("Select PLY_Id, PLY_Name, PLY_Email, PLY_Phone, PLY_Quote, PLY_Obs, PLY_Photo from tblPlayer WHERE PLY_Id = '$playerID'");
			
		if (!$query){
			//print_r("Erro de Execução.\n");
			$errorMessage = $connection->errorInfo();
			writeErrorLog($errorMessage);						
		}
		else {
			$row = $query->fetch();
			
			if (($row['PLY_Photo'] == null) or ($row['PLY_Photo'] == "")){
				$row['PLY_Photo'] = "./magicPhotos/default.jpg";
			}
			else {
				if (!file_exists($row['PLY_Photo'])) {$row['PLY_Photo'] = "./magicPhotos/default.jpg";}
			}	
			return $row;
		}
		
	}	
	
	function saveMagicDetails($row) {
		/*
		include('./assets/misc/misc.inc');
		
		$connection = new PDO("mysql:host=$host;dbname=$database;charset=utf8",$user,$password);
		$connection->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES 'UTF8'");
		*/
		$connection = getDatabaseConnection();		
		$query = $connection->prepare("UPDATE tblPlayer SET PLY_Name = :playerName, PLY_DateModify = :playerDateModify, PLY_Email = :playerEmail, PLY_Phone = :playerPhone, PLY_Quote = :playerQuote, PLY_Obs = :playerObs, PLY_Photo = :playerPhoto WHERE PLY_Id = :playerId");
		
		$destPath = savePlayerPhoto($row['PLY_Photo']);
		
		$numRows = $query->execute(array(
			"playerName" => $row['PLY_Name'],
			"playerDateModify" => date("Y-m-d"),
			"playerEmail" => $row['PLY_Email'],
			"playerPhone" => $row['PLY_Phone'],
			"playerQuote" => $row['PLY_Quote'],
			"playerObs" => $row['PLY_Obs'],
			"playerId" => $row['PLY_Id'],
			"playerPhoto" => $destPath,
		));
		
		if (!$numRows) {
			//print_r("Erro de Execução.\n"); 
			$errorMessage = $query->errorInfo();
			writeErrorLog($errorMessage);			
		}
		else {
			$username = @$_SESSION['user'];
			writeDataBaseLog($username, 'Alterar', 'Alterado Jogador '.$row['PLY_Id'].', '.$row['PLY_Name'], 1);	
					
			header("Location: viewEditPlayers.php");
		}
	}		
	
	function savePlayerPhoto($playerPhoto) {
		global $BASE_DIR;

		$destDir = "./magicPhotos/";
		$photoDir = $playerPhoto;
		$photoFile = pathinfo($photoDir);
		$photoName = $photoFile['basename'];
		$destPath = $destDir.$photoName;
		
		//unlink($destPath);
		if (rename($photoDir,$destPath)) {
			$tempDir = $destDir . "TEMP/";
			//array_map('unlink', glob("$tempDir/*"));
			unlink($tempDir);
			
			return $destPath;
		}	
		else {return $photoDir;}
		
	}
	
	function loadUserPhoto($hasPlayer, $playerPhoto, $playerId, $playerName){
		global $BASE_DIR;

		if ($hasPlayer) {
			$editPlayerPhoto = $playerPhoto;
			$editPlayerId = $playerId;
			
			$uploaddir = "./magicPhotos/TEMP/";
			$photoPostValue = $editPlayerPhoto;
			if (isset($_FILES['userfile']['name'])) {
				//Valida se extenção de ficheiro é jpg
				/* Classe finfo nao existe no servidor atual
				$finfo = new finfo(FILEINFO_MIME_TYPE);
				if (false === $ext = array_search(
					$finfo->file($_FILES['userfile']['tmp_name']),
					array(
						'jpg' => 'image/jpeg',
					),
					true
				)) {
					echo "<br><p class=\"redInformation\"> Tipo de imagem Inválida. Use formato jpg. </p><br>";
				}
				*/
				if (strtolower(pathinfo($_FILES['userfile']['name'],PATHINFO_EXTENSION)) <> 'jpg') {
					echo "<br><p class=\"redInformation\"> Tipo de imagem Inválida. Use formato jpg. </p><br>";
				}
				//Valida se o tamanho é menor que 1mb
				else if ($_FILES['userfile']['size'] > 1000000){
					echo "<br><p class=\"redInformation\"> Tamanho de imagem Inválido. Use imagens menores que 1mb.</p><br>";
				}
				else {
					$uploadfile = $uploaddir . "ui".$editPlayerId . $playerName[0].$playerName[strlen($playerName) - 1].".jpg";
					
					//array_map('unlink', glob("$uploaddir/*"));
					if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
						$photoPostValue = $uploadfile;
					} 
				}
			}
			return $photoPostValue;
		}
		
		return nothing;
	}
	
	function saveUserDetails($row, $hasPlayer) {
		/*
		include('./assets/misc/misc.inc');
		
		$connection = new PDO("mysql:host=$host;dbname=$database;charset=utf8",$user,$password);
		$connection->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES 'UTF8'");
		*/
		$connection = getDatabaseConnection();
		$useUsername = $row['USE_USERNAME'];
		$dateModify = date("Y-m-d");
		
		$query = $connection->prepare("UPDATE tblUser SET USE_USERNAME = '$useUsername', USE_DateModify = '$dateModify' WHERE USE_USERNAME = '$useUsername'");
		
		$numRows = $query->execute();
		
		if (!$numRows) {
			$errorMessage = $query->errorInfo();
			writeErrorLog($errorMessage);
		} 
		else {
			$username = @$_SESSION['user'];
			writeDataBaseLog($username, 'Alterar', 'Alterado Utilizador (Por Jogador)'.$useUsername, 1);
		}
		
		if ($hasPlayer){
					
			$query = $connection->prepare("UPDATE tblPlayer SET PLY_Name = :playerName, PLY_Email = :playerEmail, PLY_Phone = :playerPhone, PLY_Quote = :playerQuote, PLY_DateModify = :playerDateModify, PLY_Obs = :playerObs, PLY_Photo = :playerPhoto WHERE PLY_Id = :playerId");
			
			$destPath = savePlayerPhoto($row['PLY_Photo']);
			
			$numRows =	$query->execute(array(
				"playerName" => $row['PLY_Name'],
				"playerEmail" => $row['PLY_Email'],
				"playerPhone" => $row['PLY_Phone'],
				"playerQuote" => $row['PLY_Quote'],
				"playerDateModify" => $dateModify,
				"playerObs" => $row['PLY_Obs'],
				"playerId" => $row['PLY_Id'],
				"playerPhoto" => $destPath
			));	
			
			if (!$numRows) {
				$errorMessage = $query->errorInfo();
				writeErrorLog($errorMessage);
				return $errorMessage;
			}
			else {
				$username = @$_SESSION['user'];
				writeDataBaseLog($username, 'Alterar', 'Alterado Jogador (Por Jogador)'.$row['PLY_Id'].', '.$row['PLY_Name'], 1);				
			}
		}	
	}
	
	function deletePlayer($playerId) {
		
		$_retVal = -1;
		/*
		include('./assets/misc/misc.inc');
		
		$connection = new PDO("mysql:host=$host;dbname=$database;charset=utf8",$user,$password);
		$connection->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES 'UTF8'");
		*/
		
		if (!is_numeric($playerId)) {return -1;}
		
		$connection = getDatabaseConnection();
		
		$_retVal = deleteStats($playerId, "");
		
		if ($_retVal == 0) {
						
			$_retVal = setPlayerNull($playerId);	
			
			if ($_retVal == 0) {
			
				$query = $connection->query("DELETE FROM tblPlayer WHERE PLY_ID = $playerId");
				
				if (!$query){
					$errorMessage = $connection->errorInfo();
					
					writeErrorLog($errorMessage);
					
					$_retVal = $errorMessage;
				}
				else {
					$username = @$_SESSION['user'];
					writeDataBaseLog($username, 'Apagar', 'Apagado Jogador '.$playerId, 1);
				}
			}
		}
		
		return $_retVal;
		
	}	
	
	/* FIM FUNCOES DE JOGADORES */ 		
	
	/*   FUNCOES DE UTILIZADORES */	

	function saveManagerDetails($row) {
		/*
		include('./assets/misc/misc.inc');
		
		$connection = new PDO("mysql:host=$host;dbname=$database;charset=utf8",$user,$password);
		$connection->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES 'UTF8'");
		*/
		
		$connection = getDatabaseConnection();
		
		$useUsername = utf8_decode($row['USE_USERNAME']);
		$dateModify = date("Y-m-d");
		
		$query = $connection->prepare("UPDATE tblUser SET USE_USERNAME = :useUsername, USE_DateModify = :dateModify, USE_Active = :useActive, USE_Group = :useGroup, USE_Player = :usePlayer WHERE USE_Id = :useId");
		
		$playerId = $row['USE_Player'];
		
		if (($playerId <= 0) or ($playerId == "") or ($playerId == nothing)) {
			$playerId = null;
		}
			
		$numRows = $query->execute(array(
			"useUsername" => $row['USE_USERNAME'],
			"dateModify" => date("Y-m-d"),
			"useActive" => $row['USE_Active'],
			"useGroup" => $row['USE_Group'],
			"usePlayer" => $playerId,
			"useId" => $row['USE_Id'],
		));
		
		if (!$numRows) {
			$errorMessage = $query->errorInfo();
			writeErrorLog($errorMessage);			
			return $errorMessage;
		}
		else {
			$username = @$_SESSION['user'];
			writeDataBaseLog($username, 'Alterar', 'Alterado Utilizador '.$useUsername, 1);

			header("Location: viewEditUsers.php");
		}
	}	
	
	function getUserInfo($userUsername) {
		/*
		include('./assets/misc/misc.inc');
		
		$connection = new PDO("mysql:host=$host;dbname=$database;charset=utf8",$user,$password);
		$connection->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES 'UTF8'");
		*/

		$connection = getDatabaseConnection();
		$query = $connection->query("Select USE_Id, USE_USERNAME, USE_Active, USE_Group, Coalesce(USE_Player,-1) as USE_Player, PLY_Name from tblUser left join tblPlayer on USE_Player = PLY_Id WHERE USE_USERNAME='$userUsername'");
		
		if (!$query){
			$errorMessage = $connection->errorInfo();
			writeErrorLog($errorMessage);	
			return $errorMessage;
		}
		else {
			return $query->fetch();
		}
		
	}
	
	function deleteUser($userUsername) {
		/*
		include('./assets/misc/misc.inc');
		
		$connection = new PDO("mysql:host=$host;dbname=$database;charset=utf8",$user,$password);
		$connection->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES 'UTF8'");
		*/		
		if ($userUsername == "") {return -1;}
		
		$connection = getDatabaseConnection();
		$query = $connection->query("DELETE FROM tblUser WHERE USE_USERNAME = '$userUsername'");
		
		if (!$query){
			$errorMessage = $connection->errorInfo();
			writeErrorLog($errorMessage);					
			return $errorMessage;
		}
		else {
			$username = @$_SESSION['user'];
			writeDataBaseLog($username, 'Apagar', 'Apagado Utilizador '.$userUsername, 1);
			return 0;
		}
		
	}	
	
	function setPlayerNull($playerId) {
		/*
		include('./assets/misc/misc.inc');
		
		$connection = new PDO("mysql:host=$host;dbname=$database;charset=utf8",$user,$password);
		$connection->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES 'UTF8'");
		*/
		
		if ($playerId == "") {return -1;}
		
		$connection = getDatabaseConnection();
		$query = $connection->query("UPDATE tblUser SET USE_Player = null WHERE USE_Player = $playerId");
		
		if (!$query){
			$errorMessage = $connection->errorInfo();
			writeErrorLog($errorMessage);			
			return $errorMessage;
		}
		else {
			$username = @$_SESSION['user'];
			writeDataBaseLog($username, 'Alterar', 'Atribuicao Jogador Null em Utilizador '.$playerId, 1);
			return 0;
		}		
	}
	
	/*   FIM DE FUNCOES DE UTILIZADORES */
	
	/* FUNCOES DE ESTATISTICAS */

	function getStatsOrder($orderIndex, $orderType, $orderSequence) {

		$orderRatio = array(		
			"Magico" => "PLY_Name",
			"Posicao_Ant" => "Coalesce(oldQuery.oldPosition,1000)",
			"Batalhas" => "COUNT(STA_GameDate)",
			"Golos" => "ROUND(SUM(STA_Goals)/COUNT(STA_GameDate),2)",
			"Assistencias" => "ROUND(SUM(STA_Assists)/COUNT(STA_GameDate),2)",
			"Vitorias" => "ROUND(SUM(STA_Victory = 1)/COUNT(STA_GameDate),2)",
			"Traicoes" => "ROUND(SUM(STA_OwnGoals)/COUNT(STA_GameDate),2)",
			"Assiduidade" => "ROUND(100 * (COUNT(STA_GameDate)/TotalJogos.Jogos),0)"
		);
		
		$orderAbsolute = array(		
			"Magico" => "PLY_Name",
			"Posicao_Ant" => "Coalesce(oldQuery.oldPosition,1000)",
			"Batalhas" => "COUNT(STA_GameDate)",
			"Golos" => "SUM(STA_Goals)",
			"Assistencias" => "SUM(STA_Assists)",
			"Vitorias" => "SUM(STA_Victory = 1)",
			"Traicoes" => "SUM(STA_OwnGoals)",
			"Assiduidade" => "ROUND(100 * (COUNT(STA_GameDate)/TotalJogos.Jogos),0)"
		);
		
		$fieldType = "ratio";
		$fieldOrder = "Golos";
		$fieldSequence = " DESC";

		if ($orderSequence == "up") {$fieldSequence = " ASC";}

		if ($orderIndex != null) {
			$fieldOrder = $orderIndex;
		}

		if ($orderType != null) {
			$fieldType = $orderType;
		}		
		
		if ($fieldType == "Absoluto"){
			$strWhere = $orderAbsolute[$fieldOrder].$fieldSequence;
		}
		else {
			$strWhere = $orderRatio[$fieldOrder].$fieldSequence;	
		}

		return $strWhere;
	}

	function getStatsInfo($statsPlayer, $statsDate) {
		/*
		include('./assets/misc/misc.inc');
		
		$connection = new PDO("mysql:host=$host;dbname=$database;charset=utf8",$user,$password);
		$connection->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES 'UTF8'");
		*/
		$connection = getDatabaseConnection();
		$query = $connection->query("Select * FROM tblStats WHERE STA_Player=$statsPlayer and STA_GameDate = '$statsDate'");
		
		if (!$query){
			//print_r("Erro de Execução\n");
			$errorMessage = $connection->errorInfo();
			writeErrorLog($errorMessage);		
		}
		else {
			return $query->fetch();
		}
		
	}	
	
	function listStatsDetails($editing, $row) {
			
			$statsDate = $row['STA_GameDate'];
			$statsPlayer = $row['STA_Player'];
			$statsGoals = $row['STA_Goals'];
			$statsAssists = $row['STA_Assists'];
			$statsOwnGoals = $row['STA_OwnGoals'];
			$statsVictory = $row['STA_Victory'];
			$statsColor = $row['STA_Color'];
			$statsPayed = $row['STA_Payed'];
			$statsObs = utf8_encode($row['STA_Obs']);	
			
			if (!$editing) {			
				echo "<input type=\"hidden\" name=\"statsDate\" id=\"statsDate\" value=\"$statsDate\">\n";
			}
			else {
				echo "<label for=\"statsDate\">Data Batalha:</label>\n";
				echo "<input type=\"date\" id=\"statsDate\" name=\"statsDate\" value=\"$statsDate\" readonly >\n";
			}

			echo "<br><label for=\"statsPlayer\">Mágico:</label>\n";
			echo "<select class=\"default\" name=\"statsPlayer\"";
			if ($editing) {echo " readonly ";}
			echo "}>\n";
			$playerName = getPlayerName($statsPlayer);
			if ($playerName <> -1) {
				echo "<option value=\"$statsPlayer\">$playerName</option>";
			}
			
			if (!$editing){
				$result = getPlayerListNoStat($statsDate);
				foreach ($result as $row)
				{
					$playerID =$row['PLY_ID'];
					$playerName = utf8_encode($row['PLY_Name']);			
					echo "<option value=\"$playerID\">$playerName";
					if ($playerID == $statsPlayer) {echo " selected ";}
					echo "</option>";
				}
			}
			echo "</select>";

			echo "<br>\n<label for=\"statsGoals\">Golos:</label>";
			echo "<input type=\"number\" id=\"statsGoals\" required name=\"statsGoals\" min = \"0\" max=\"100\" value=\"$statsGoals\">\n";

			echo "<br>\n<label for=\"statsAssists\">Assistências:</label>\n";
			echo "<input type=\"number\" id=\"statsAssists\" required name=\"statsAssists\" min = \"0\" max=\"100\" value=\"$statsAssists\">\n";

			echo "<br>\n<label for=\"statsOwnGoals\">Auto-Golos:</label>\n";
			echo "<input type=\"number\" id=\"statsOwnGoals\" required name=\"statsOwnGoals\" min = \"0\" max=\"10\" value=\"$statsOwnGoals\">\n";

			echo "<br>\n<input type=\"checkbox\" id=\"statsVictory\" name=\"statsVictory\" value=\"1\"";
			if ($statsVictory == 1) { echo " checked ";}
			echo ">\n<label for=\"statsVictory\"><span></span><strong>Ganhou Batalha</strong></label>";

			echo "<br>\n<label for=\"statsObs\">Observações:</label>\n";
			echo "<input type=\"text\" id=\"statsObs\" name=\"statsObs\" value = \"$statsObs\">\n";

			echo "<br>\n<label for=\"statsColor\">Cor:</label>\n";
			echo "<select name=\"statsColor\" class=\"default\">\n<option value=\"Preto\"";
			if ($statsColor == "Preto") {echo " selected ";}
			echo ">Preto</option>\n<option value=\"Branco\"";
			if ($statsColor == "Branco") {echo " selected ";}
			echo ">Branco</option>\n</select>\n";

			echo "<br>\n<input type=\"checkbox\" id=\"statsPayed\" name=\"statsPayed\" value=\"1\"";
			if ($statsPayed == 1) { echo " checked ";}
			echo "><label for=\"statsPayed\"><span></span><strong>Pagou Jogo</strong></label>";
				
			if ($editing){
				echo "<br>\n<input name=\"btnAction\" type=\"submit\" value=\"Guardar\">";
			}
			else {
				echo "<br>\n<input name=\"btnAction\" type=\"submit\" value=\"Adicionar\">";	
			}
	}			
	
	function saveStatisticDetails($row) {
		/*
		include('./assets/misc/misc.inc');
		
		$connection = new PDO("mysql:host=$host;dbname=$database;charset=utf8",$user,$password);
		$connection->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES 'UTF8'");
		*/
		
		$statsPlayer = $row['STA_Player'];
		$statsDate = $row['STA_GameDate'];
				
		if ((is_numeric($statsPlayer)) and ($statsDate <> '' )){
			

			$connection = getDatabaseConnection();
			$query = $connection->prepare("UPDATE tblStats SET STA_Goals = :statsGoals, STA_DateModify = :statsDateModify, STA_Assists = :statsAssists, STA_OwnGoals = :statsOwnGoals, STA_Payed = :statsPayed, STA_Victory = :statsVictory, STA_Color = :statsColor, Sta_Obs = :statsObs WHERE STA_Player = :statsPlayer and STA_GameDate = :statsDate");

			$numRows = $query->execute(array(
				"statsGoals" => $row['STA_Goals'],
				"statsDateModify" => date("Y-m-d"),
				"statsAssists" => $row['STA_Assists'],
				"statsOwnGoals" => $row['STA_OwnGoals'],
				"statsVictory" => $row['STA_Victory'],
				"statsColor" => $row['STA_Color'],
				"statsPayed" => $row['STA_Payed'],
				"statsObs" => $row['STA_Obs'],
				"statsPlayer" => $statsPlayer,
				"statsDate" => $statsDate 
			));
			
			if (!$numRows) {
				//print_r("Erro de Execução.\n"); 
				$errorMessage = $query->errorInfo();
				writeErrorLog($errorMessage);					
			}
			else {
				$username = @$_SESSION['user'];
				writeDataBaseLog($username, 'Alterar', 'Alterada Estatística '.$statsPlayer.', '.$statsDate, 1);
				header("Location: viewEditStatistics.php");
			}
		}
		else{
			echo "<br><p class=\"redInformation\"> Erro ao carregar par Jogador/Data para atualização de valores. </p><br>";
		}
	}			
	
	function getStatsTable ($type, $strWhere)
	{
		/*
		include('./assets/misc/misc.inc');
		
		$connection = new PDO("mysql:host=$host;dbname=$database;charset=utf8",$user,$password);
		$connection->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES 'UTF8'");
		*/
		$connection = getDatabaseConnection();
		$stringQuery =  "SELECT @curRow := @curRow + 1 AS 'Posição' ,  t1.PLY_Name as 'Mágico', ";
		if ($type == 0) {
			$stringQuery = $stringQuery."Coalesce(oldQuery.oldPosition,1000) as 'Posição Anterior', ";
			
		}
		$stringQuery = $stringQuery."Batalhas, Golos, Assistencias as 'Assistências', Vitorias as 'Vitórias' ";
		if ($type == 0) {
			$stringQuery = $stringQuery.", Traicoes as 'Traições', Assiduidade ";
		}
		$stringQuery = $stringQuery."FROM
							( SELECT
							PLY_NAME , COUNT(STA_GameDate) as 'Batalhas',
							 CONCAT(SUM(STA_Goals),' (' , ROUND(SUM(STA_Goals)/COUNT(STA_GameDate),2), ')') as 'Golos', 
							 CONCAT(SUM(STA_Assists),' (', ROUND(SUM(STA_Assists)/COUNT(STA_GameDate),2),')') as 'Assistencias',
							 CONCAT(SUM(STA_Victory = 1), ' (', ROUND(SUM(STA_Victory = 1)/COUNT(STA_GameDate),2),')') as 'Vitorias',
							 CONCAT(SUM(STA_OwnGoals),' (', ROUND(SUM(STA_OwnGoals)/COUNT(STA_GameDate),2), ')') as  'Traicoes',
							 CONCAT(ROUND(100 * (COUNT(STA_GameDate)/TotalJogos.Jogos),0),'%') as 'Assiduidade', 
							ROUND(100 * (COUNT(STA_GameDate)/TotalJogos.Jogos),0) as tempAss
							FROM tblStats
							inner Join tblPlayer on PLY_ID = STA_Player
							inner Join (SELECT COUNT(DISTINCT STA_GameDate) as 'Jogos' FROM tblStats) as TotalJogos
							GROUP BY PLY_NAME
							HAVING tempAss >= 25
							ORDER BY $strWhere
							) t1";
		if ($type == 0) {
			$stringQuery = $stringQuery." LEFT OUTER JOIN (
							SELECT @oldRow := @oldRow + 1 AS oldPosition, playerName FROM
							( SELECT
							PLY_NAME as playerName,
							ROUND(100 * (COUNT(STA_GameDate)/TotalJogos.Jogos),0) as tempAss
							FROM tblStats
							inner Join tblPlayer on PLY_ID = STA_Player
							inner Join (SELECT COUNT(DISTINCT STA_GameDate) as 'Jogos' FROM tblStats) as TotalJogos
							WHERE STA_GameDate < (SELECT MAX(STA_GameDate) FROM tblStats)
							GROUP BY PLY_NAME
							HAVING tempAss >= 25
							ORDER BY $strWhere
							) t3, (SELECT @oldRow:=0) t4
							) oldQuery
							on playerName = PLY_Name";
		}
		$stringQuery = $stringQuery.", (SELECT @curRow:=0) t2";
				
		$query = $connection->query($stringQuery);
		
		if (! $query) {
			$errorMessage = $connection->errorInfo();
			writeErrorLog($errorMessage);				
		}
		else {
			
			$column_count = $query->columnCount();
				
			echo "<div class=\"ovScroll\">\n";
			echo "<table class=\"default\" style=\"width:100%\">\n";
			echo "<tr>\n";
			for ($counter = 0; $counter < $column_count; $counter++) {
				$meta = $query->getColumnMeta($counter);
				$columnName = $meta['name'];
				echo "<th>$columnName</th>\n";
				if (($counter == 0) and ($type == 0)){echo "<th></th>\n";} //Insere coluna para indicação de subida de posição
			}
			echo "</tr>\n";
			$result = $query->fetchALL();
		
			foreach ($result as $row) {
				echo "<tr>\n";
				for ($counter = 0; $counter < $column_count; $counter++) {
					$value = utf8_encode($row[$counter]);
					echo "<td>$value";
					echo "</td>\n";
					if (($counter == 0)and ($type == 0)){
						echo "<td>";
						//Insere indicação de subida de posição
						if ($row[$counter] > $row[2]){
							echo " <div class=\"arrow-down-red\"></div>";
						}
						elseif ($row[$counter] < $row[2]) {
							echo " <div class=\"arrow-up-green\"></div>";
						}
						echo "</td>\n";
					}
				}
				echo "</tr>\n";
			}
			echo "</table>\n";
			echo "</div>\n";
		}
		
		if ($type == 0){
				/*Imprime os jogadores fora das estatísticas*/
	
			$stringQuery = "SELECT '-' AS 'Posição' , t1.PLY_Name as 'Mágico', Batalhas, Golos, 
							Assistencias as 'Assistências', Vitorias as 'Vitórias', Traicoes as 'Traições', Assiduidade FROM
							( SELECT
							PLY_NAME , COUNT(STA_GameDate) as 'Batalhas',
							 CONCAT(SUM(STA_Goals),' (' , ROUND(SUM(STA_Goals)/COUNT(STA_GameDate),2), ')') as 'Golos', 
							 CONCAT(SUM(STA_Assists),' (', ROUND(SUM(STA_Assists)/COUNT(STA_GameDate),2),')') as 'Assistencias',
							 CONCAT(SUM(STA_Victory = 1), ' (', ROUND(SUM(STA_Victory = 1)/COUNT(STA_GameDate),2),')') as 'Vitorias',
							 CONCAT(SUM(STA_OwnGoals),' (', ROUND(SUM(STA_OwnGoals)/COUNT(STA_GameDate),2), ')') as  'Traicoes',
							 CONCAT(ROUND(100 * (COUNT(STA_GameDate)/TotalJogos.Jogos),0),'%') as 'Assiduidade', 
							ROUND(100 * (COUNT(STA_GameDate)/TotalJogos.Jogos),0) as tempAss
							FROM tblStats
							inner Join tblPlayer on PLY_ID = STA_Player
							inner Join (SELECT COUNT(DISTINCT STA_GameDate) as 'Jogos' FROM tblStats) as TotalJogos
							GROUP BY PLY_NAME
							HAVING tempAss < 25
							ORDER BY ROUND(SUM(STA_Goals)/COUNT(STA_GameDate),2) DESC
							) t1";
							
			$query = $connection->query($stringQuery);
			
			if (! $query) {
				$errorMessage = $connection->errorInfo();
				writeErrorLog($errorMessage);					
			}
			else {
				
				$column_count = $query->columnCount();
				$result = $query->fetchALL();
				
				if (count($result) > 0) {
					echo "<br>\n<br>\n<header>\n
								\t<h2>Mágicos de Fora da Classificação</h2>\n
								</header>\n<br>\n";
					
					echo "<div class=\"ovScroll\">\n";
					echo "<table class=\"default\" style=\"width:100%\">\n";
					echo "<tr>\n";
					for ($counter = 0; $counter < $column_count; $counter++) {
						$meta = $query->getColumnMeta($counter);
						$columnName = $meta['name'];
						echo "<th>$columnName</th>\n";
					}
					echo "</tr>\n";
					
				
					foreach ($result as $row) {
						echo "<tr>\n";
						for ($counter = 0; $counter < $column_count; $counter++) {
							$value = utf8_encode($row[$counter]);
							echo "<td>$value</td>\n";
						}
						echo "</tr>\n";
					}
					echo "</table>\n";
					echo "</div>\n";
				}
			}
		}
		$query = null;
		$connection = null;
	}
	
	function getPlayerAssiduity($playerID) {
		/*
		include('./assets/misc/misc.inc');
		
		$connection = new PDO("mysql:host=$host;dbname=$database;charset=utf8",$user,$password);
		$connection->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES 'UTF8'");
		*/
		$connection = getDatabaseConnection();		
		$query = $connection->query("SELECT CONCAT(Coalesce(ROUND(100 * (COUNT(STA_GameDate)/TotalJogos.Jogos),0),0),'%') as 'Assiduidade' from tblStats inner Join (SELECT COUNT(DISTINCT STA_GameDate) as 'Jogos' FROM tblStats) as TotalJogos WHERE STA_Player = $playerID");
		
		if (!$query){
			$errorMessage = $connection->errorInfo();
			writeErrorLog($errorMessage);
			
			$query = null;
			$connection = null;
			return 0;
		}
		else {
			return $query->fetchColumn();
			$query = null;
			$connection = null;
		}		
		
	}

	function deleteStats($statsPlayer, $statsDate) {
		/*
		include('./assets/misc/misc.inc');
		
		$connection = new PDO("mysql:host=$host;dbname=$database;charset=utf8",$user,$password);
		$connection->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES 'UTF8'");
		*/
		
		if (!(is_numeric($statsPlayer))) {return -1;}

		$connection = getDatabaseConnection();
		
		if ($statsDate == "") {
			$query = $connection->query("DELETE FROM tblStats WHERE STA_Player = $statsPlayer");
		}
		else {	
			$query = $connection->query("DELETE FROM tblStats WHERE STA_Player = $statsPlayer and STA_GameDate = '$statsDate'");
		}
		
		if (!$query){
			$errorMessage = $connection->errorInfo();			
			writeErrorLog($errorMessage);
			
			return $errorMessage;
		}
		else {
			$username = @$_SESSION['user'];
			writeDataBaseLog($username, 'Apagado', 'Apagada Estatistica '.$statsPlayer.', '.$statsDate, 1);
			return 0;
		}
		
	}	
	
	/* FIM DE FUNCOES DE ESTATISTICAS */
	
	function writeErrorLog($messageErro, $initPath = ".") {
		global $BASE_DIR;

		$file = $BASE_DIR."/Error/log_".date("Y-m-d").".txt";

		for ($i = 0; $i < $messageErro; $i++)
		{
			file_put_contents($file, $messageErro[i]."\n\n", FILE_APPEND);
		}
	}

	function writeDataBaseLog($username, $action, $message, $type) {
		$connection = getDatabaseConnection();

		$query = $connection->prepare("INSERT INTO tblLog (log_User,Log_Action,Log_Date,Log_MSG,LOG_Type) VALUES (:logUser,:logAction,:logDate,:logMSG,:logType)");

		$numRows = $query->execute(array(
			"logUser" => utf8_decode($username),
			"logAction" => utf8_decode($action),
			"logDate" => date("Y-m-d"),
			"logMSG" => utf8_decode($message),
			"logType" => $type
		));

		if (!$numRows) {
			$errorMessage = $query->errorInfo();
			writeErrorLog($errorMessage);					
		}		
	}
	
?>