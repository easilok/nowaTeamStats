//For fancy checkbox
$(document).ready(function(){
  
  $('input[type=checkbox]').tzCheckbox({labels:['Enable','Disable']});
});

(function (global) {

  var nowateam = {};

  var jsonGetTable = "/json/stats/JSON-Stats-Table.php";
  var jsonGetTableOff = "/json/stats/JSON-Stats-Off-Table.php";
  var statsRowRatioHtml = "/assets/snippets/stats/statistic-table-full-ratio-snippet.html";
  var statsRowAbsoluteHtml = "/assets/snippets/stats/statistic-table-full-absolute-snippet.html";
  var statsRowHeaderHtml = "/assets/snippets/stats/statistic-table-full-header-snippet.html";
  var statsOffRowRatioHtml = "/assets/snippets/stats/statistic-table-off-full-ratio-snippet.html";
  var statsOffRowAbsoluteHtml = "/assets/snippets/stats/statistic-table-off-full-absolute-snippet.html";
  var statsOffRowHeaderHtml = "/assets/snippets/stats/statistic-table-off-full-header-snippet.html";

  // On page load (before images or CSS)
  document.addEventListener("DOMContentLoaded", function (event) {

    nowateam.getStatsTable = function(orderName, orderType, orderSequence) {
      var postArguments = new Object();
      postArguments['fieldType'] = orderType;
      postArguments['fieldOrder'] = $documentUtils.getColumnOrder(orderName);
      postArguments['fieldSequence'] = orderSequence;


      var statsRowSnippet = statsRowRatioHtml;
      if (orderType === 'Absoluto') {
        statsRowSnippet = statsRowAbsoluteHtml;
      }

      // On load, show table
      $documentUtils.showLoading("#full-table-stats");
      $documentUtils.insertHtml('#main-off-stats', "");

      $ajaxUtils.sendPostRequest(
        jsonGetTable,
        function (statistics) {
          $ajaxUtils.sendGetRequest(
            statsRowHeaderHtml,
            function(statsRowHeaderHtml) {
              $ajaxUtils.sendGetRequest(
                statsRowSnippet,
                function(statsRowHtml) {
                  if (statistics['Error'] == undefined){
                    var statsViewHtml = 
                      buildStatsTableHtml(statistics,statsRowHeaderHtml,statsRowHtml);

                    $documentUtils.insertHtml('#full-table-stats', statsViewHtml);

                    setColumnOrder(orderName, orderSequence);

                    nowateam.getStatsOFFTable(orderName, orderType, orderSequence);
                  } else {
                    $documentUtils.insertHtml('#full-table-stats', "");
                  }
                },
            false);//Get Row Snippet
            },
            false); //Get RowHeader
        },
        postArguments,
        true); //Get Statistics
    };

    nowateam.getStatsTable("player-goals", "racio", "down");

    nowateam.getStatsOFFTable = function(orderName, orderType, orderSequence) {
      var postArguments = new Object();
      postArguments['fieldType'] = orderType;
      postArguments['fieldOrder'] = $documentUtils.getColumnOrder(orderName);
      postArguments['fieldSequence'] = orderSequence;


      var statsOffRowSnippet = statsOffRowRatioHtml;
      if (orderType === 'Absoluto') {
        statsOffRowSnippet = statsOffRowAbsoluteHtml;
      }

      // On load, show table
      $documentUtils.showLoading("#main-off-stats");

      $ajaxUtils.sendPostRequest(
        jsonGetTableOff,
        function (statisticsOff) {
          $ajaxUtils.sendGetRequest(
            statsOffRowHeaderHtml,
            function(statsOffRowHeaderHtml) {
              $ajaxUtils.sendGetRequest(
                statsOffRowSnippet,
                function(statsOffRowHtml) {
                  if (statisticsOff['Error'] == undefined){
                    var statsOffViewHtml = 
                      buildStatsOffTableHtml(statisticsOff,statsOffRowHeaderHtml,statsOffRowHtml);

                    $documentUtils.insertHtml('#main-off-stats', statsOffViewHtml);
                  } else {
                    $documentUtils.insertHtml('#main-off-stats', "");
                  }
                },
            false);//Get Row Snippet
            },
            false); //Get RowHeader
        },
        postArguments,
        true); //Get Statistics
    };

    function buildStatsTableHtml(statistics, statsRowHeaderHtml,
                                  statsRowHtml){
      var finalHtml = "";

      var html = statsRowHeaderHtml;
      for (var key in statistics[0]) {
        
        var columnHeader = $documentUtils.getColumnHeader(key);
        var smallColumnHeader = $documentUtils.getSmallColumnHeader(key);
        
        html = $documentUtils.insertProperty(html,key,
                columnHeader);
        html = $documentUtils.insertProperty(html,"short_" + key,
                smallColumnHeader);
      }
      finalHtml += html + "\n";

      //Loop over stats
      for (var i = 0; i < statistics.length; i++){
        html = statsRowHtml;
        for (var key in statistics[i]) {
          html = $documentUtils.insertProperty(html,key,statistics[i][key]);
        }
        positionArrow = '';
        if (parseInt(statistics[i]['Posicao'], 10) > parseInt(statistics[i]['Posicao_Ant'], 10)) {
          positionArrow = '<i class="fa fa-caret-down" aria-hidden="true" style="color: red;"></i>';
        } else if (parseInt(statistics[i]['Posicao'], 10) < parseInt(statistics[i]['Posicao_Ant'], 10)) {
          positionArrow = '<i class="fa fa-caret-up" aria-hidden="true" style="color: green;"></i>';
        }
        html = $documentUtils.insertProperty(html,"Position_arrow",positionArrow);
        
        finalHtml += html + "\n";
      }
      return finalHtml;
    }

    function buildStatsOffTableHtml(statistics, statsRowHeaderHtml,
                                  statsRowHtml){
      var finalHtml = "";

      var html = statsRowHeaderHtml;
      for (var key in statistics[0]) {
        var columnHeader = $documentUtils.getColumnHeader(key);
        var smallColumnHeader = $documentUtils.getSmallColumnHeader(key);

        html = $documentUtils.insertProperty(html,key,
                columnHeader);
        html = $documentUtils.insertProperty(html,"short_" + key,
                smallColumnHeader);        
      }
      finalHtml = html + "\n";

      //Loop over stats
      var tableHtml ="";
      for (var i = 0; i < statistics.length; i++){
        html = statsRowHtml;
        for (var key in statistics[i]) {
          html = $documentUtils.insertProperty(html,key,statistics[i][key]);
        }        
        tableHtml += html + "\n";
      }

      finalHtml = $documentUtils.insertProperty(finalHtml,"rows_here",tableHtml);

      return finalHtml;
    }    

    // Click event for column header to order
    nowateam.orderByColumn = function(orderName){
      //Default order is DESC
      var orderSequence = "down";
      // Get order type (Absolute/Ratio)
      var orderType = document.querySelector("#order-type").value;

      if ((orderName != undefined) && (orderName != null)){
        //Check if it's an order change
        var columnCurrentOrder = document
                                .querySelector('#' + orderName + '-order')
                                .querySelector('i');
        if (columnCurrentOrder != null) {
          if (columnCurrentOrder.className.indexOf("fa-caret-down") !=-1 ){
            orderSequence = "up";
          }
        }
        console.log(orderName);
        nowateam.getStatsTable(orderName, orderType, orderSequence);
      }
    }    

    // Remove all arrows from all columns
    function removeAllOrderArrows (){

        // // Select all possible columns with order arrows
        // var headers = document.querySelectorAll(".column-header");

        // for (var i = 0; i < headers.length; i++) {
        //   $documentUtils.insertHtml('#' + headers[i].id + '-order','');
        // }                      
    }    

    // Put arrow indicating table order
    function setColumnOrder (columnId, order) {
     
      // Check if the column sent is an acceptable value
      if (document.querySelector('#' + columnId + '-order') != null){

        // Remove arrows from order before
        removeAllOrderArrows();

        //Set new arrow for new order
        if (order === 'up') {
          $documentUtils.insertHtml('#' + columnId + '-order',
            '<i class="fa fa-caret-up leftSpace" aria-hidden="true"></i>');
        } else {
          $documentUtils.insertHtml('#' + columnId + '-order',
            '<i class="fa fa-caret-down leftSpace" aria-hidden="true"></i>');
        }
      }
    }

  }); //Close DOMContentLoaded
  global.$nowateam = nowateam;

})(window);
