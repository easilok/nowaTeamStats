/**** Ecra Completo ****/
/**** EQUIPA PRETA ****/
//Primeira coluna: left:100px
//Segunda coluna: left:250px

/**** EQUIPA Branca ****/
//Primeira coluna: left:380px
//Segunda coluna: left:530px

/**** Linhas ****/
// 3 jogadores: top: 50px; spacing 150px;
// 2 jogadores: top: 125px; spacing 150px;
// 1 jogador: top: 200px;

/**** Organizacao ****/
// 3 jogadores = 2 + 1
// 4 jogadores = 3 + 1
// 5 jogadores = 3 + 2
// 6 jogadores = 3 + 3
/**** Ecra Completo ****/

/**** Ecra Medio *****/
/**** EQUIPA PRETA ****/
//Primeira linha: top:90px
//Segunda linha: top:180px
//Terceira linha: top:270px

/**** EQUIPA Branca ****/
//Primeira linha: top:420px
//Segunda linha: top:510px
//Terceira linha: top:600px

/**** Colunas ****/
// 2 jogadores: left: 60px; spacing 210px;
// 1 jogador: top: 160px;

/**** Organizacao ****/
// 3 jogadores = 2 + 1
// 4 jogadores = 2 + 2
// 5 jogadores = 2 + 2 + 1
// 6 jogadores = 2 + 2 + 1
/**** Ecra Medio *****/

/**** Ecra Pequeno *****/
/**** EQUIPA PRETA ****/
//Primeira linha: top:50px
//Segunda linha: top:120px
//Terceira linha: top:190px

/**** EQUIPA Branca ****/
//Primeira linha: top:310px
//Segunda linha: top:380px
//Terceira linha: top:450px

/**** Colunas ****/
// 2 jogadores: left: 30px; spacing 130px;
// 1 jogador: top: 110px;

/**** Organizacao ****/
// 3 jogadores = 2 + 1
// 4 jogadores = 2 + 2
// 5 jogadores = 2 + 2 + 1
// 6 jogadores = 2 + 2 + 1
/**** Ecra Pequeno *****/

// Start Function
$(document).ready(function(){

  //Variables
  var jsonGetTeam = "/json/battle-replay/JSON-GET-Team.php";
  var htmlBlackBox = "/assets/snippets/battle/blackTeamBox.html"
  var htmlWhiteBox = "/assets/snippets/battle/whiteTeamBox.html"

  var blackTeamPlayers = new Object();
  var whiteTeamPlayers = new Object();
  var blackTeamBox = "";
  var whiteTeamBox = "";

  var blackTeamPlayed = new Object();
  var whiteTeamPlayed = new Object();

  getPlayers();

  $(window).resize(getTeams);

  function getPlayers () {
    
    $.get(htmlBlackBox, function(htmlBlack) {
      blackTeamBox = htmlBlack;

      $.get(htmlWhiteBox, function (htmlWhite) {

        whiteTeamBox = htmlWhite;
        var postArg = {
          statsColor : 'Preto'
        }

        $.post(jsonGetTeam, postArg, function (data) {
          
          blackTeamPlayers = data;

          postArg['statsColor'] = 'Branco';

          $.post(jsonGetTeam, postArg, function (data) {
          
            whiteTeamPlayers = data;

            getTeams()

          }, 'json');
        }, 'json');
      });
    });

  }

  function getTeams () {

    var blackTeamLength = $.map(blackTeamPlayers, function(n, i) { return i; }).length;
    var whiteTeamLength = $.map(whiteTeamPlayers, function(n, i) { return i; }).length;
    if ((blackTeamPlayers === undefined) || (whiteTeamPlayers === undefined) || (blackTeamLength === 0) || (whiteTeamLength === 0)) {return;}

    if ($("#small-size-indicator").is(":visible") || $("#medium-size-indicator").is(":visible")) {
      blackTeamPlayed = fillTeamSmall(blackTeamPlayers, 0);
    } else {
      blackTeamPlayed = fillTeamFull(blackTeamPlayers, 0);
    }

    if ($("#small-size-indicator").is(":visible") || $("#medium-size-indicator").is(":visible")) {
      whiteTeamPlayed = fillTeamSmall(whiteTeamPlayers, 1);
    } else {
      whiteTeamPlayed = fillTeamFull(whiteTeamPlayers, 1);	        
    }
			       
    drawTeam(blackTeamBox, blackTeamPlayed, whiteTeamBox, whiteTeamPlayed);

  }

	function drawTeam(htmlBlack, blackTeamPlayed, htmlWhite, whiteTeamPlayed) {
		
		var htmlField = "";
		var html = "";
		
		var blackTeamPlayedLength = $.map(blackTeamPlayed, function(n, i) { return i; }).length;

		for (var i = 0; i < blackTeamPlayedLength; i++) {
			html = htmlBlack;
			
			var styleCode = "top: " +  blackTeamPlayed[i]['top'] + "px; left: " + blackTeamPlayed[i]['left'] + "px";
			var boxText = blackTeamPlayed[i]['player'] + "\n<br>\n" + blackTeamPlayed[i]['stats'];

			html = $documentUtils.insertProperty(html, "style", styleCode);
			html = $documentUtils.insertProperty(html, "boxText", boxText);

			htmlField += html + "\n";
		}

		var whiteTeamPlayedLength = $.map(whiteTeamPlayed, function(n, i) { return i; }).length;

		for (var i = 0; i < whiteTeamPlayedLength; i++) {
			html = htmlWhite;

			var styleCode = "top: " +  whiteTeamPlayed[i]['top'] + "px; left: " + whiteTeamPlayed[i]['left'] + "px";
			var boxText = whiteTeamPlayed[i]['player'] + "\n<br>\n" + whiteTeamPlayed[i]['stats'];

			html = $documentUtils.insertProperty(html, "style", styleCode);
			html = $documentUtils.insertProperty(html, "boxText", boxText);

			htmlField += html + "\n";
		}

		$('#field').html(htmlField);
	}

  function getRowNumberFull (numberOfPlayers, color) {
  	var rowNumber = new Object();
  		if (numberOfPlayers == 3) {
  			rowNumber['1'] = 2;
  		} else {
  			rowNumber['1'] = 3;
  		}

  		if (numberOfPlayers < 5) {
  			rowNumber['2'] = 1;
  		} else {
  			rowNumber['2'] = numberOfPlayers - 3;
  		}

  		if (color == 1)  {
  			var temp = rowNumber['1'];
  			rowNumber['1'] = rowNumber['2'];
  			rowNumber['2'] = temp;
  		}

  		return rowNumber;
  }

 function getColumnNumberSmall (numberOfPlayers, color) {
      /**** Organizacao ****/
    // 3 jogadores = 2 + 1
    // 4 jogadores = 2 + 2
    // 5 jogadores = 2 + 2 + 1
    // 6 jogadores = 2 + 2 + 2
    var columnNumber = new Object();

    if (numberOfPlayers > 1) {
      columnNumber['1'] = 2;
    } else {
      columnNumber['1'] = numberOfPlayers;
    }

    if (numberOfPlayers > 2) {
      if (numberOfPlayers < 4){
        columnNumber['2'] = 1;
      } else {
        columnNumber['2'] = 2;
      }
    } else {
      columnNumber['2'] = 0;
    }

    if (numberOfPlayers > 4) {
      columnNumber['3'] = numberOfPlayers - 4;
    } else {
      columnNumber['3'] = 0;
    }

    if (color == 1)  {
      var temp = columnNumber['1'];
      columnNumber['1'] = columnNumber['3'];
      columnNumber['3'] = temp;
    }

      return columnNumber;
  }  

  function getFirstLine (numberOfPlayers) {
  	/**** Linhas ****/
  	// 3 jogadores: top: 50px; spacing 150px;
  	// 2 jogadores: top: 125px; spacing 150px;
  	// 1 jogador: top: 200px;  	

  	var pos = 50;

  	if (numberOfPlayers == 2) {
  		pos = 125;
  	} else if (numberOfPlayers == 1) {
  		pos = 200;
  	}

  	return pos;
  }

  function fillTeamFull(data, color) {

  	rowNumber = getRowNumberFull(data.length, color);

  	var Team = new Object();
    if((data === undefined) || (data.length == 0)){return Team;}
  	var linepos = getFirstLine(rowNumber['1']);

  	//Primeira Coluna
  	for (var i = 0; i < rowNumber['1']; i++){
  		Team[i] = {
  			'top' : linepos,
  			'left' : ((color == 0) ? 100 : 380),
  			'player' : data[i]['PLY_Name'],
  			'stats' : 'G:' + data[i]['STA_Goals'] + ' A:' + data[i]['STA_Assists'] + ' T:' + data[i]['STA_OwnGoals'],
  		};
  		linepos += 150;
  	}

	linepos = getFirstLine(rowNumber['2']);
  	//Segunda Coluna
  	for (var i = 0; i < rowNumber['2']; i++){
  		Team[i + rowNumber['1']] = {
  			'top' : linepos,
  			'left' : ((color == 0) ? 250 : 530),
  			'player' : data[i + rowNumber['1']]['PLY_Name'],
  			'stats' : 'G:' + data[i + rowNumber['1']]['STA_Goals'] + ' A:' + data[i + rowNumber['1']]['STA_Assists'] + ' T:' + data[i + rowNumber['1']]['STA_OwnGoals']
  		};
  		linepos += 150;
  	}

  	return Team;
  }

  function getFirstColumn (numberOfPlayers) {
    /**** Linhas ****/
    //Small
    // 2 jogadores: left: 30px; spacing 160px;
    // 1 jogador: top: 110px;
    //Medium
    // 2 jogadores: left: 60px; spacing 210px;
    // 1 jogador: top: 160px;

    var pos = [0, 0]; 

    if ($("#medium-size-indicator").is(":visible")) {
      pos = [160 - 210, 210];
      
      if (numberOfPlayers == 2) {
        pos[0] = 60 - pos[1];
      }      
    } else {

      pos = [110 - 160, 160];

      if (numberOfPlayers == 2) {
        pos[0] = 30 - pos[1];
      }      
    }

    return pos;
  }

  function fillTeamSmall(data, color) {

    /**** Ecra Pequeno *****/
    /**** EQUIPA PRETA ****/
    //Primeira linha: top:50px
    //Segunda linha: top:120px
    //Terceira linha: top:190px    
    /**** EQUIPA Branca ****/
    //Primeira linha: top:310px
    //Segunda linha: top:380px
    //Terceira linha: top:450px
    /**** ECra Medio *****/
    /**** EQUIPA PRETA ****/
    //Primeira linha: top:90px
    //Segunda linha: top:180px
    //Terceira linha: top:270px

    /**** EQUIPA Branca ****/
    //Primeira linha: top:420px
    //Segunda linha: top:510px
    //Terceira linha: top:600px
    if ($("#medium-size-indicator").is(":visible")) {
      if (color === 0) {
        linepos = [90, 180, 270];
      } else {
        linepos = [420, 510, 600];
      }
    } else {
      if (color === 0) {
        linepos = [50, 120, 190];
      } else {
        linepos = [310, 380, 450];
      }    
    }

    rowNumber = getColumnNumberSmall(data.length, color);

    var Team = new Object();
    if((data === undefined) || (data.length == 0)){return Team;}

    var actualPlayer = 0;
    var leftPos = [0, 0];

    for (var j = 1; j < 4; j++) {

      leftPos = getFirstColumn(rowNumber[j]);
      var pos = leftPos[0];

      for (var i = 0; i < rowNumber[j]; i++) {
        pos += leftPos[1];
        Team[actualPlayer] = {
          'top' : linepos[j - 1],
          'left' : pos,
          'player' : data[actualPlayer]['PLY_Name'],
          'stats' : 'G:' + data[actualPlayer]['STA_Goals'] + ' A:' + data[actualPlayer]['STA_Assists'] + ' T:' + data[actualPlayer]['STA_OwnGoals'],                 
        }        
        
        actualPlayer++;
        if (actualPlayer > data.length) {break;}
      }
    }
    return Team;
  }

});