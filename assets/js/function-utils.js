(function (global) {
	var documentUtils = {};

  // Convenience function for inserting innerHTML for 'select'
  documentUtils.insertHtml = function (selector, html) {
    var targetElem = document.querySelector(selector);
    targetElem.innerHTML = html;
  };

  // Show loading icon inside element identified by 'selector'.
  documentUtils.showLoading = function (selector) {
    var html = "<div class='text-center'>";
    html += "<img src='/images/ajax-loader.gif'></div>";
    documentUtils.insertHtml(selector, html);
  };

  // Return substitute of '{{propName}}'
  // with propValue in given 'string'
  documentUtils.insertProperty = function (string, propName, propValue) {
    var propToReplace = "{{" + propName + "}}";
    string = string
      .replace(new RegExp(propToReplace, "g"), propValue);
    return string;
  };

  documentUtils.getColumnHeader = function (keyName){
    switch(keyName) {
      case "Posicao":
        return "Posição";
        break;
      case "Magico":
        return "Mágico";
        break;
      case "Posicao_Ant":
        return "Anterior";
        break;
      case "Batalhas":
        return "Batalhas";
        break;
      case "Golos":
        return "Golos";
        break;        
      case "Golos_Rat":
        return "Golos (%)";
        break;
      case "Assistencias":
        return "Assistências";
        break;     
      case "Assistencias_Rat":
        return "Assistências (%)";
        break;
      case "Vitorias":
        return "Vitórias";
        break;  
      case "Vitorias_Rat":
        return "Vitórias (%)";
        break;  
      case "Traicoes":
        return "Traições";
        break;  
      case "Traicoes_Rat":
        return "Traições (%)";
        break;  
      case "Assiduidade":
        return "Assiduidade";
        break;     
      default:
        return null;
    }
  }  

  documentUtils.getSmallColumnHeader = function (keyName){
    switch(keyName) {
      case "Posicao":
        return "PS";
        break;
      case "Magico":
        return "Mágico";
        break;
      case "Posicao_Ant":
        return "AT";
        break;
      case "Batalhas":
        return "BT";
        break;
      case "Golos":
        return "GL";
        break;        
      case "Golos_Rat":
        return "GL (%)";
        break;
      case "Assistencias":
        return "AS";
        break;     
      case "Assistencias_Rat":
        return "AS (%)";
        break;
      case "Vitorias":
        return "VT";
        break;  
      case "Vitorias_Rat":
        return "VT (%)";
        break;  
      case "Traicoes":
        return "TR";
        break;  
      case "Traicoes_Rat":
        return "TR (%)";
        break;  
      case "Assiduidade":
        return "AD";
        break;     
      default:
        return null;
    }
  }  

  documentUtils.getColumnOrder = function (columnName){
    switch(columnName) {
      case "player-name":
        return "Magico";
        break;
      case "player-before":
        return "Posicao_Ant";
        break;
      case "player-battles":
        return "Batalhas";
        break;
      case "player-goals":
        return "Golos";
        break;        
      case "Golos_Rat":
        return "Golos_Rat";
        break;
      case "player-assists":
        return "Assistencias";
        break;     
      case "Assistencias_Rat":
        return "Assistencias_Rat";
        break;
      case "player-victories":
        return "Vitorias";
        break;  
      case "Vitorias_Rat":
        return "Vitorias_Rat";
        break;  
      case "player-betray":
        return "Traicoes";
        break;  
      case "Traicoes_Rat":
        return "Traicoes_Rat";
        break;  
      case "player-assiduity":
        return "Assiduidade";
        break;  
      default:
        return null;        
    }
  }    

	// Expose utility to the global object
	global.$documentUtils = documentUtils;

})(window);