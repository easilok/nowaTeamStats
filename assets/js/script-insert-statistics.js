// Start Function
$(document).ready(function(){

  //Variables
  var jsonGetPlayers = "/json/stats/JSON-Get-Players.php";
  var jsonGetAddedPlayers = "/json/stats/JSON-Added-Magics.php";
  var jsonSaveStats = "/json/stats/JSON-Insert-Stat.php";

  //Date Picker Startup
  var today = new Date();
  if (today.getDay() != 6) {
    today.setTime( today.getTime() - (today.getDay() + 1) * 86400000 );
  }

  document.getElementById("statsDate").valueAsDate = today;
  datePickerChange(); 
  $("#statsDate").on("change", datePickerChange);
  $("#btnAddStats").on("click", saveNewStats);

  //Date Picker Change Function
  function datePickerChange() {

    checkWeekDay();

    fillDatePlayers();

  }

  function fillDatePlayers () {
    var postArg = {
      statsDate : $("#statsDate").val()
    }

    $.post(jsonGetPlayers, postArg, function (data) {
        fillPlayersSelect(data);
    }, 'json');

    $.post(jsonGetAddedPlayers, postArg, function (data) {
        fillAddedPlayers(data);
    }, 'json');
  }

  function fillPlayersSelect(playersList) {
    $documentUtils.showLoading("#statsPlayer");  
    
    var htmlOptions = '';

    if (playersList.length > 0 ) {

      for (var i = 0; i < playersList.length; i++){
        htmlOptions += "<option value='" + playersList[i]['PLY_ID'] + "'>" + playersList[i]['PLY_Name'] + "</option>\n";
      }

      $('#statsPlayer').html(htmlOptions);

      lockData(0);

    } else {
      lockData(1);
    }
  }

  function fillAddedPlayers(playersList) {
    $documentUtils.showLoading("#magicsAdded");  
    
    var htmlOptions = '';

    for (var i = 0; i < playersList.length; i++){
      htmlOptions += "- " + playersList[i]['PLY_Name'] + ";\n<br>\n";
    }

    $('#magicsAdded').html(htmlOptions);
  }

  function lockData (type){
    if (type === 1) {
      $("#statsPlayer").attr('readonly', true);
      $("#statsGoals").attr('readonly', true);
      $("#statsAssists").attr('readonly', true);
      $("#statsOwnGoals").attr('readonly', true);
      $("#statsVictory").attr('readonly', true);
      $("#statsColor").attr('readonly', true);
      $("#statsPayed").attr('readonly', true);
      $("#statsObs").attr('readonly', true);
      $("#btnAddStats").attr('readonly', true);      
      $("#errorHolder").text('Não existem Jogadores disponíveis para adicionar neste dia!');
    } else {
      $("#statsPlayer").attr('readonly', false);
      $("#statsGoals").attr('readonly', false);
      $("#statsAssists").attr('readonly', false);
      $("#statsOwnGoals").attr('readonly', false);
      $("#statsVictory").attr('readonly', false);
      $("#statsColor").attr('readonly', false);
      $("#statsPayed").attr('readonly', false);
      $("#statsObs").attr('readonly', false);
      $("#btnAddStats").attr('readonly', false);   
      $("#errorHolder").text('');
    }
  }

  function cleanData (type) {
    $("#statsGoals").val(0);
    $("#statsAssists").val(0);
    $("#statsOwnGoals").val(0);
    $("#statsObs").text("");
    if (type === 1) {
      $("#statsColor").val(0).change();
      $('#statsPayed')[0].checked = true;
      $('#statsVictory')[0].checked = false;
    }
  }

  //Check WeekDay for Saturday
  function checkWeekDay() {

  	var dateSelected = new Date($("#statsDate").val());	
  	if (dateSelected.getDay() != 6) {
  		$("#statsDate").css("border-color", "red");

  		$("#dateError").text("A Data Selecionada não corresponde a um sábado!");
      $("#dateError").addClass("give-some-top-space");
      //alert("A Data Selecionada não corresponde a um sábado!");
  	} else {
  		$("#statsDate").css("border-color", "");
      $("#dateError").removeClass("give-some-top-space");
      $("#dateError").text("");
  	}

  }

  function saveNewStats() {
    
    $("body").addClass("loading");

    var saveArg = {
      statsDate : $("#statsDate").val(),
      statsPlayer : $("#statsPlayer").val(),
      statsGoals : $("#statsGoals").val(),
      statsAssists : $("#statsAssists").val(),
      statsOwnGoals : $("#statsOwnGoals").val(),
      statsObs : $("#statsObs").val(),
      statsPayed : document.getElementById('statsPayed').checked ? 1 : 0,
      statsVictory : document.getElementById('statsVictory').checked ? 1 : 0,
      statsColor : $("#statsColor").val()
    } 

    
    $.post(jsonSaveStats, saveArg, function (data) {

        if (data['Added'] === true) {
          $("#errorHolder").text("Estatística Inserida.");
          cleanData(0);          
        } else if (data['AlreadyExists'] === true) {
          $("#errorHolder").text("Estatística Já Existente.");
        } else {
          $("#errorHolder").text("Erro ao Inserir Estatística.");
        }

        fillDatePlayers();

        document.getElementById('pageHeader').scrollIntoView();

        $("body").removeClass("loading");

    }, 'json');    

  }

});