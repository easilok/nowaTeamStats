(function (global) {

  var loginObj = {};

  var jsonCheckLogin = "/json/login/JSON-Check-Login.php";
  var jsonRecoverPass = "/json/login/JSON-Recover-Password.php";
  var jsonChangePassword = "/json/login/JSON-Change-Password.php";
  var snipLogin = "/assets/snippets/login/login-snipped.html";
  var snipRecover = "/assets/snippets/login/login-pass-recover-snipped.html";
  var snipChangePass = "/assets/snippets/login/login-change-password-snipped.html";

  // On page load (before images or CSS)
  document.addEventListener("DOMContentLoaded", function (event) {

    // Login Page
    var loginViewHTML = function (event, username, errorHolder) {
      // On load, show table
      $documentUtils.showLoading("#login-content");

      document.querySelector('#bodyTitle')
              .textContent = "Insira as Credenciais de Manager";

      $ajaxUtils.sendGetRequest(
        snipLogin,
        function(LoginHtml) {
          var LoginHtml = 
            $documentUtils.insertHtml('#login-content', LoginHtml);

            // Button Click events
            document.querySelector("#btnLogin")
              .addEventListener("click", processLoginCommand);
            document.querySelector("#btnRecover")
              .addEventListener("click", recoverViewHtml);  

            //Enter Events on Textbox      
            document.querySelector("#usernameField")
              .addEventListener("keyup", function(event){
                  if(event.keyCode == 13){
                    processLoginCommand();
                  }
              });
            document.querySelector("#userPassword")
              .addEventListener("keyup", function(event){
                  if(event.keyCode == 13){
                    processLoginCommand();
                  }
              });              

            //Pre define username
            if (username != undefined) {
              document.querySelector("#usernameField")
                      .value = username;  
            }
        },
        false);
    }

    loginViewHTML();
    document.querySelector('#errorHolder')
            .textContent = "";    

    var processLoginCommand = function (event) {
      var username = document.querySelector("#usernameField").value;
      var Password = document.querySelector("#userPassword").value;
      $documentUtils.showLoading("#login-content");

      var postArguments = new Object();
      postArguments['username'] = username;
      postArguments['userPassword'] = Password;

      $ajaxUtils.sendPostRequest(
          jsonCheckLogin,
          function(loginInfo) {
            if (loginInfo['Login'] === true){
              window.open('index.php',"_self");
            } else if (loginInfo['LoginNotFound'] === true) {
              //open login com informação de erro
              loginViewHTML();
              document.querySelector('#errorHolder')
                      .textContent = "Conjunto Password incorrecto.";
            } else if (loginInfo['PasswordExpired'] === true){
              changePasswordViewHtml(username);
              document.querySelector('#errorHolder')
                      .textContent = "";
            } else {
              loginViewHTML();
              document.querySelector('#errorHolder')
                      .textContent = "Ocorreu um erro no Login. Tente Novamente.";
            }
          },
          postArguments,
          true);           
    }

    // Recover Password Page
    var recoverViewHtml = function (event) {
      // On load, show table
      var username = document.querySelector("#usernameField").value;
      var email = null;
      if (document.querySelector("#recoverEmail") != null) {
        email = document.querySelector("#recoverEmail").value;
      }
      $documentUtils.showLoading("#login-content");
      document.querySelector('#bodyTitle')
              .textContent = "Recuperar Palavra-Passe";

      $ajaxUtils.sendGetRequest(
          snipRecover,
          function(recoverHtml) {
            var recoverHtml = 
              $documentUtils.insertHtml('#login-content', recoverHtml);
              document.querySelector("#usernameField")
                      .value = username;                
              if (email != null) {
                document.querySelector("#recoverEmail")
                        .value = email;                
              }
              document.querySelector("#btnRecoverEmail")
                      .addEventListener("click", processRecoverCommand);

              //Enter Events on Textbox      
              document.querySelector("#usernameField")
                .addEventListener("keyup", function(event){
                    if(event.keyCode == 13){
                      processRecoverCommand();
                    }
                });
              document.querySelector("#recoverEmail")
                .addEventListener("keyup", function(event){
                    if(event.keyCode == 13){
                      processRecoverCommand();
                    }
                });                

              if (event != undefined){
                document.querySelector('#errorHolder')
                      .textContent = "";
              }
          },
          false);
    }

    var processRecoverCommand = function (event) {
      var username = document.querySelector("#usernameField").value;
      var email = document.querySelector("#recoverEmail").value;
      //$documentUtils.showLoading("#login-content");

      var postArguments = new Object();
      postArguments['userUsername'] = username;
      postArguments['recoverEmail'] = email;

      $ajaxUtils.sendPostRequest(
          jsonRecoverPass,
          function(recoverPassInfo) {
            if (recoverPassInfo['sent'] === true){
              //Email enviado
              //Reabre login com informação para login com a pass enviada
              loginViewHTML(null, username);            
              document.querySelector('#errorHolder')
                      .textContent = "Email com Recuperação de Palavra-Passe enviado. Por favor efetuar Login com a Palavra-Passe enviada.";
            } else if (recoverPassInfo['invalidEmail'] === true) {
              //Reabre pagina
              //Adiciona erro que email é invalido
              recoverViewHtml();
              document.querySelector('#errorHolder')
                      .textContent = "Email fornecido é inválido.";
            } else if (recoverPassInfo['userNotFound'] === true){
              //Reabre pagina
              //Adiciona erro que não tem tamanho certo         
              recoverViewHtml();
              document.querySelector('#errorHolder')
                      .textContent = "Utilizador não encontrado.";
            } else {
              //Abre login com indicação de que aconteceu erro
              //Adiciona erro que não tem tamanho certo                 
              loginViewHTML(null, username);
              document.querySelector('#errorHolder')
                      .textContent = "Erro ao enviar email de recuperação.";              
            }
          },
          postArguments,
          true);         
    }

    // Change Password Page 
    var changePasswordViewHtml = function (username) {
      // On load, show table
      $documentUtils.showLoading("#login-content");

      $ajaxUtils.sendGetRequest(
          snipChangePass,
          function(changePassHtml) {
            var changePassHtml = 
              $documentUtils.insertHtml('#login-content', changePassHtml);
              document.querySelector("#usernameField").value = username;                

              document.querySelector("#btnChangePassword")
                      .addEventListener("click", processChangePasswordCommand);

              //Enter Events on Textbox      
              document.querySelector("#usernameField")
                .addEventListener("keyup", function(event){
                    if(event.keyCode == 13){
                      processChangePasswordCommand();
                    }
                });
              document.querySelector("#userPassword")
                .addEventListener("keyup", function(event){
                    if(event.keyCode == 13){
                      processChangePasswordCommand();
                    }
                });
              document.querySelector("#userRepeatPassword")
                .addEventListener("keyup", function(event){
                    if(event.keyCode == 13){
                      processChangePasswordCommand();
                    }
                });                                
          },
          false);

      document.querySelector('#bodyTitle')
              .textContent = "Palavra-Passe Expirada! Alterar Palavra-Passe.";      
    }

    var processChangePasswordCommand = function (event) {
      var username = document.querySelector("#usernameField").value;
      var Password = document.querySelector("#userPassword").value;
      var repeatPassword = document.querySelector("#userRepeatPassword").value;
      $documentUtils.showLoading("#login-content");

      var postArguments = new Object();
      postArguments['userUsername'] = username;
      postArguments['userPassword'] = Password;
      postArguments['userPasswordRepeat'] = repeatPassword;

      $ajaxUtils.sendPostRequest(
          jsonChangePassword,
          function(changePassInfo) {
            if (changePassInfo['Changed'] === true){
              //Alterado com sucesso
              //Reabre login para re-efetuar login
              loginViewHTML(null, username);            
              document.querySelector('#errorHolder')
                      .textContent = "Palavra-Passe alterada com sucesso. Voltar a efetuar o Login.";
            } else if (changePassInfo['PasswordSize'] === true) {
              //Reabre pagina
              //Adiciona erro que não tem tamanho certo
              changePasswordViewHtml(username);
              document.querySelector('#errorHolder')
                      .textContent = "Palavra-Passe tem de possuir mais de 6 carateres.";
            } else if (changePassInfo['diffPassword'] === true){
              //Reabre pagina
              //Adiciona erro que não tem tamanho certo         
              changePasswordViewHtml(username);
              document.querySelector('#errorHolder')
                      .textContent = "Palavras-Passe não coincidem.";
            } else if (changePassInfo['userNotFound'] === true){
              //Abre login com indicação de que utilizador nao existe
              //Adiciona erro que não tem tamanho certo
              loginViewHTML(null, username);
              document.querySelector('#errorHolder')
                      .textContent = "Utilizador desconhecido.";
            } else {
              //Abre login com indicação de que aconteceu erro
              //Adiciona erro que não tem tamanho certo                 
              loginViewHTML(null, username);
              document.querySelector('#errorHolder')
                      .textContent = "Erro ao alterar Palavra-Passe.";              
            }
          },
          postArguments,
          true);       
    }    

  });

  global.$loginObj = loginObj;

})(window);
