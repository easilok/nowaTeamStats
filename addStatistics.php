<?php
	
	session_start();
		
	if ((@$_SESSION['auth'] != "yes") or (@$_SESSION['group'] < 3)){
		header("Location: index.php");
		exit();
	}	
	
?>

<!DOCTYPE HTML>
<!--
	Helios by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>NowaTeam Estatísticas Detalhadas</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<link rel="icon" href="./images/Nowabrand.png">
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
			<?php 	header('Content-Type: text/html; charset=utf-8'); ?>
			<script src="//cdn.jsdelivr.net/webshim/1.14.5/polyfiller.js"></script>
			<script>
				webshims.setOptions('forms-ext', {types: 'date'});
				webshims.polyfill('forms forms-ext');
			</script>
	</head>
	<body class="no-sidebar">
		<div id="page-wrapper">

			<!-- Header -->
				<div id="header">

					<!-- Inner -->
						<div class="inner">
							<header>
								<h1><a href="index.php" id="logo">NowaTeam</a></h1>
							</header>
						</div>

					<!-- Nav -->
					<?php include 'navigationMenu.php'; ?>

				</div>

			<!-- Main -->
				<div class="wrapper style1">

					<div class="container">
						<div class="row 200%">
						<!-- Lado esquerdo-->
							<div class="8u 12u(mobile)" id="content">
								<article id="main">
									<header>
										<h2><a href="#">Adicionar Estatísticas de Mágico</a></h2>
									</header>
									<p>
									
										<form action="addStatistics.php" method="POST">
											<label for="statsDate">Data Batalha:</label>
											<input type="date" id="statsDate" name="statsDate" onChange="form.submit()"
											<?php 
												if (isset($_POST['statsDate'])) {
													$statsDate = $_POST['statsDate'];
												} 
												else {
													$statsDate = date("Y-m-d");
												} 
												echo "value=\"$statsDate\"";
												?>>
										</form>

										<?php include ('addStatistics2Database.php'); ?>
															
										<form action="addStatistics.php" method="POST">
											<fieldset>
												<!--<legend>Estatística de Mágico</legend>-->
												<br>
													<input type="hidden" name="statsDate" id="statsDate" value=<?php echo "\"$statsDate\"" ?>>
													
													<label for="statsPlayer">Mágico:</label>
													<?php include("listPlayers.php"); ?>
													
													<br>
													
													<label for="statsGoals">Golos:</label>
													<input type="number" id="statsGoals" required name="statsGoals" min = "0" max="100" value="0">
													
													<br>
													
													<label for="statsAssists">Assistências:</label>
													<input type="number" id="statsAssists" required name="statsAssists" min = "0" max="100" value="0">

													<br>
													
													<label for="statsOwnGoals">Auto-Golos:</label>
													<input type="number" id="statsOwnGoals" required name="statsOwnGoals" min = "0" max="10" value="0">											

													<br>
												
													<input type="checkbox" id="statsVictory" name="statsVictory" value ="True"
													<?php 
														if (isset($statsVictory)) {
															if ($statsVictory == "True") { echo "checked";}
														}  
													?>  > 				
													
													<label for="statsVictory"><span></span><strong>Ganhou Batalha</strong></label>
													
													<label for="statsObs">Observações:</label>
													<input type="text" id="statsObs" name="statsObs">		

													<br>

													<label for="statsColor">Cor:</label>
													<select name = "statsColor"  class="default">
														<option value="Preto" <?php  
															if (isset($statsColor)) {
																if ($statsColor == "Preto") {echo "selected";}
															}  
															else {
																echo "selected";
															} 
															?> >Preto</option>
														<option value="Branco" <?php  
															if (isset($statsColor)) {
																if ($statsColor == "Branco") {echo "selected";}
															}  
															?> >Branco</option>
													</select>													

													<br>													
													
													<!--<label for="statsPayed">Pago:</label>-->
													<input type="checkbox" id="statsPayed" name="statsPayed" value ="True" 
													<?php 
														if (isset($statsPayed)) {
															if ($statsPayed == "True") { echo "checked";}
														}  
														else { echo "checked";}
													?>  > 				
													<label for="statsPayed"><span></span><strong>Pagou Jogo</strong></label>
												
													<br>
													<input type="submit" name="btnAction" value="Adicionar">
													
											</fieldset>
											<br>
										</form>
									</p>	
								</article>
							</div>
							<!-- Lado direito-->
							<div class="4u 12u(mobile)" id="sidebar">
								<hr class="first" />
								<section>
									<header>
										<h3>Mágicos Já Pontuados</h3>
									</header>
									<p>
										<?php include ('listPlayerStatistics.php') ?>
									</p>
									<footer>
										<a href="statistics.php" class="button">Ver Estatísticas Totais</a>
									</footer>
								</section>
								<hr />
							</div>
						</div>
					</div>

				</div>

			<!-- Footer -->
			<?php include 'footerInclude.php'; ?>
		
		</div>

		<?php include_once('./assets/includes/utils/incScripts.php'); ?>

	</body>
</html>