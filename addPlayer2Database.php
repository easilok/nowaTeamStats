<?php

	//include('./assets/misc/misc.inc');
	
	if (isset($_POST['playerName'])) {		
			
		$playerName = ucfirst(strip_tags(trim($_POST['playerName'])));
		$playerPhone = (strip_tags(trim($_POST['playerPhone'])));
		$playerEmail = (strip_tags(trim($_POST['playerEmail'])));
		$playerQuote = (strip_tags(trim($_POST['playerQuote'])));
		
		/*
		$connection = new PDO("mysql:host=$host;dbname=$database;charset=utf8",$user,$password);
		$connection->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES 'UTF8'");
		*/
		include_once("functions.php");
		$connection = getDatabaseConnection();
		$query = $connection->query("Select COUNT(PLY_ID) from tblPlayer where PLY_Name = '$playerName'");
		
		if (! $query) {
			echo "<br><p class=\"redInformation\"> Erro de Execução:</p><br>\n";
			$errorMessage = $connection->errorInfo();
			writeErrorLog($errorMessage);
		}
		else {
			$count = $query->fetchColumn();
					
			if ($count > 0 ) {
				echo "<br><p class=\"redInformation\"> Nome de Mágico Já está registado. Escolher outro nome.</p><br>";
			}
			
			else {

				$query = $connection->prepare("INSERT INTO tblPlayer (PLY_Name,PLY_Email,PLY_Phone,PLY_RegisterDate,PLY_Quote,PLY_Obs) VALUES (:playerName,:playerEmail,:playerPhone,:registrationDate,:playerQuote,:playerObs)");

				$numRows = $query->execute(array(
					"playerName" => utf8_decode($playerName),
					"playerEmail" => $playerEmail,
					"playerPhone" => $playerPhone,
					"registrationDate" => date("Y-m-d"),
					"playerQuote" => utf8_decode($playerQuote),
					"playerObs" => ""
				));
				
				if (!$numRows) {
					echo "<br><p class=\"redInformation\"> Erro ao Inserir Mágico: $playerName </p><br>";
					$errorMessage = $query->errorInfo();
					writeErrorLog($errorMessage);	
				}
				else {
		
					echo "<br><p class=\"redInformation\"> Inserido Mágico: $playerName </p><br>";
					
					$playerName = "";
					$playerPhone = "";
					$playerEmail = "";
					$playerQuote = "Sem Imaginação!";

					$username = @$_SESSION['user'];
					writeDataBaseLog($username, 'Inserir', 'Inserido Jogador'.$playerName, 1);
				}
				
			}
		}
	}
		
?>